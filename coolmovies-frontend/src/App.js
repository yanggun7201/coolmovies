import React, { useEffect } from 'react';
import { css, Global } from "@emotion/react";
import { BrowserRouter } from "react-router-dom";
import Header from "./components/Header";
import Routes from "./core/routes/Routes";
import { gql, useQuery } from "@apollo/client";
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Modal from 'react-modal';
import { useCurrentUserContext } from "./core/contexts/CurrentUserContext";
import Loading from "./components/loading/Loading";

Modal.setAppElement('#root');

const App = () => {
    const { data, loading } = useQuery(GET_USER);
    const { setUser } = useCurrentUserContext();

    useEffect(() => {
        if (data) {
            setUser(data.currentUser);
        }
    }, [data, setUser]);
    return (
        <div css={containerStyle}>
            <Global styles={style} />
            <ToastContainer newestOnTop />
            {loading
                ? (<Loading onTop overlay />)
                : (
                    <BrowserRouter>
                        <Header />
                        <Routes />
                    </BrowserRouter>
                )
            }
        </div>
    );
}

const GET_USER = gql`
    query getCurrentUser {
        currentUser {
            id
            name
        }
    }
`;

const containerStyle = (theme) => css`
    display: flex;
    flex-direction: column;
    max-width: 1344px;
    position: relative;
    min-height: 300px;

    ${theme.breakpoints.up("xl")} {
        margin: 0 auto;
    }

    ${theme.breakpoints.down("lg")} {
        flex-direction: column;
    }
`;

const style = (theme) => css`
    body {
        padding: 0;
        margin: 0;
        background-color: ${theme.colours.background};
        color: ${theme.colours.text};
    }

    a {
        text-decoration: none;
        color: ${theme.colours.link};

        :active,
        :focus {
            color: ${theme.colours.link};
        }
    }

    * {
        margin: 0;
        padding: 0;
        font-family: '"Source Sans Pro",sans-serif';
    }

    *,
    :after,
    :before {
        box-sizing: border-box;
    }

    .page {
        position: absolute;
    }

    .page-enter {
        opacity: 0;
        transform: scale(1.1);
    }

    .page-enter-active {
        opacity: 1;
        transform: scale(1);
        transition: opacity 300ms, transform 300ms;
    }

    .page-exit {
        opacity: 1;
        transform: scale(1);
    }

    .page-exit-active {
        opacity: 0;
        transform: scale(0.9);
        transition: opacity 300ms, transform 300ms;
    }

    .ReactModal__Content.ReactModal__Content--after-open {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }
`;

export default App;
