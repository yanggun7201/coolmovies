import React, { memo } from 'react';
import { css } from "@emotion/react";
import { Link } from "react-router-dom";
import { ReactComponent as LogoSVG } from "../images/logo.svg";
import { ReactComponent as LogoBackgroundSVG } from "../images/logo-background.svg";
import CommonLink from "./links/CommonLink";
import { useCurrentUserContext } from "../core/contexts/CurrentUserContext";

const Header = () => {
    const { user } = useCurrentUserContext();

    return (
        <div css={style}>
            <LogoBackgroundSVG css={backgroundStyle} />
            <Link to={"/"}><LogoSVG css={logoStyle} /></Link>
            <div css={createReviewLinkStyle}>
                <CommonLink to={`/reviewers/${user.id}`}><span>My reviews</span></CommonLink>
                <CommonLink to={"/reviews"}><span>Reviews</span></CommonLink>
                <CommonLink to={"/movies"}><span>Movies</span></CommonLink>
            </div>
        </div>
    )
};

const style = css`
    position: relative;
    max-height: calc(100vw / 4);
`;

const logoStyle = css`
    position: absolute;
    width: 300px;
    top: 10px;
    left: 10px;
    max-width: calc(100vw / 2.5);
    height: auto;
`;

const backgroundStyle = css`
    max-width: calc(100vw / 2);
    height: auto;
`;

const createReviewLinkStyle = css`
    position: absolute;
    top: 10px;
    right: 10px;
    max-width: calc(100vw / 2);
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: flex-end;

    > a {
        margin-left: 20px;
    }

    > a:not(:first-of-type) {
        ::before {
            content: '|';
            position: absolute;
            left: -10px;
        }
    }
`;

export default memo(Header);