import React, { memo, useCallback } from "react";
import { NOOP } from "../../core/constants";
import { css } from "@emotion/react";
import useSetState from "../../core/hooks/useSetState";
import LabelledField from "../form/LabelledField";
import Input from "../form/Input";
import DropDown from "../form/Dropdown";
import useDirectorsData from "../../core/hooks/useDirectorsData";

const DEFAULT_STATE = {
    title: '',
    orderBy: null,
    director: null,
};

const Filters = ({
    className = '',
    resetFilters = NOOP,
    updateFilters = NOOP,
    title = 'Filter',
    orderByOptions = [],
}) => {
    const [state, setState] = useSetState(DEFAULT_STATE);
    const allDirectors = useDirectorsData();

    const onResetClicked = useCallback(() => {
        setState({ ...DEFAULT_STATE });
        resetFilters();
    }, [resetFilters, setState]);

    const onTitleChanged = useCallback((e, name, value) => {
        setState({ [name]: value });
        updateFilters({ [name]: value }, true);
    }, [updateFilters, setState]);

    const onOrderByChanged = useCallback((e, selectedItem) => {
        setState({ orderBy: selectedItem });
        updateFilters({ orderBy: selectedItem });
    }, [updateFilters, setState]);

    const onDirectorChanged = useCallback((e, selectedItem) => {
        setState({ director: selectedItem });
        updateFilters({ director: selectedItem });
    }, [updateFilters, setState]);

    return (
        <div css={style} className={className}>
            <div css={titleContainerStyle}>
                <div css={titleStyle}>{title}</div>
                <div css={resetStyle} onClick={onResetClicked}>
                    Reset filters
                </div>
            </div>
            <div css={filtersContainerStyle}>
                <div css={filterBlockStyle}>
                    <LabelledField name="title" label="Title">
                        <Input
                            name="title"
                            onChange={onTitleChanged}
                            value={state.title}
                            hasBorder
                            autoComplete={"off"}
                        />
                    </LabelledField>
                </div>
                <div css={filterBlockStyle}>
                    <LabelledField
                        name="director"
                        label="Director"
                    >
                        <DropDown
                            selected={state.director}
                            setSelected={onDirectorChanged}
                            options={allDirectors}
                            hasBorder
                        />
                    </LabelledField>
                </div>
                <div css={filterBlockStyle}>
                    <LabelledField
                        name="orderBy"
                        label="Order by"
                    >
                        <DropDown
                            selected={state.orderBy}
                            setSelected={onOrderByChanged}
                            options={orderByOptions}
                            hasBorder
                        />
                    </LabelledField>
                </div>
            </div>
        </div>
    );
}

const style = css`
    width: 100%;
    height: 100%;
`;

const titleContainerStyle = css`
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 12px;
`;

const titleStyle = css`
    font-size: 22px;
`;

const resetStyle = theme => css`
    color: ${theme.colours.link};
    cursor: pointer;
    align-items: center;
    padding-top: 2px;

    ${theme.mixins.animatedUnderlineItSelf({
        extraStyle: css`
            :after {
                bottom: 2px;
            }
        `
    })};
`;

const filtersContainerStyle = css`
    display: flex;
    flex-wrap: wrap;
`;

const filterBlockStyle = theme => css`
    width: 33.33%;
    padding: 12px;

    ${theme.breakpoints.down("sm")} {
        width: 100%;
    }
`;

export default memo(Filters);