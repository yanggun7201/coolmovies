import React, { memo, useState } from 'react';
import PropTypes from "prop-types";
import { css } from "@emotion/react";
import { useFormik } from "formik";
import { trim } from "lodash";
import LabelledField from "../form/LabelledField";
import Textarea from "../form/Textarea";
import Button from "../form/Button";
import { NOOP } from "../../core/constants";

const MovieReviewCreateCommentForm = ({
    className = '',
    loading = false,
    onSubmit = NOOP,
}) => {
    const [isSubmitting, setIsSubmitting] = useState(false);

    const {
        values,
        handleSubmit,
        handleChange,
        isValid,
        resetForm,
    } = useFormik({
        initialValues: {
            body: '',
        },
        validateOnMount: true,
        validate,
        onSubmit: values => {
            setIsSubmitting(true);
            onSubmit(values).then(() => {
                resetForm();
            }).finally(() => {
                setIsSubmitting(false);
            });
        },
    });

    return (
        <form onSubmit={handleSubmit} css={formStyle} className={className}>
            <LabelledField name="body" label="Leave a comment">
                <Textarea
                    name="body"
                    onChange={handleChange}
                    value={values.body}
                    hasBorder
                    autoComplete={"off"}
                    css={bodyTextareaStyle}
                    {...isSubmitting && { disabled: true }}
                />
                <Button
                    disabled={!isValid}
                    loading={isSubmitting}
                >
                    Save
                </Button>
            </LabelledField>
        </form>
    );
};

const validate = values => {
    const errors = {};

    if (!trim(values.body)) {
        errors.body = 'Required';
    }
    return errors;
};

const bodyTextareaStyle = css`
    width: 100%;
    margin-bottom: 2px;
`;

const formStyle = css`
    display: flex;
    flex-direction: column;
    margin-bottom: 20px;
    height: 100%;
    width: 100%;
    position: relative;
`;

MovieReviewCreateCommentForm.propTypes = {
    onSubmit: PropTypes.func,
    loading: PropTypes.bool,
    className: PropTypes.string,
}

export default memo(MovieReviewCreateCommentForm);