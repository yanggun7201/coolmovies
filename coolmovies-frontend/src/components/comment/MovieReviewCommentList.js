import React, { memo, useCallback, useEffect, useState } from 'react';
import PropTypes from "prop-types";
import { gql, useMutation } from "@apollo/client";
import { css } from "@emotion/react";
import Loading from "../../components/loading/Loading";
import MovieReviewComment from "./MovieReviewComment";
import { useCurrentUserContext } from "../../core/contexts/CurrentUserContext";
import { errorMessage } from "../../core/hooks/useToast";
import MovieReviewCreateCommentForm from "./MovieReviewCreateCommentForm";

const MovieReviewCommentList = ({
    movieReviewId = null,
    movieReviewComments = [],
    loading = false,
    className = '',
}) => {
    const [comments, setComments] = useState([]);
    const { user } = useCurrentUserContext();
    const [createComment] = useMutation(CREATE_COMMENT_QUERY);
    const [deleteComment] = useMutation(DELETE_COMMENT_QUERY);
    const [updateComment] = useMutation(UPDATE_COMMENT_QUERY);

    const onDeleteComment = useCallback((commentNodeId) => {
        return deleteComment({
            variables: {
                input: {
                    nodeId: commentNodeId
                },
            },
        }).then(() => {
            setComments(prevComments => {
                return prevComments.filter(comment => comment.nodeId !== commentNodeId);
            });
        }).catch(() => {
            errorMessage("Comment deletion error.");
        });
    }, [deleteComment]);

    const onCreateComment = useCallback((values) => {
        return createComment({
            variables: {
                input: {
                    comment: {
                        title: '',
                        body: values.body,
                        userId: user.id,
                        movieReviewId: movieReviewId,
                    }
                }
            },
        }).then(({ data }) => {
            if (data?.createComment?.comment) {
                setComments(prevComments => {
                    return [data.createComment.comment, ...prevComments];
                });
            }
        }).catch(() => {
            errorMessage("Comment creation error.");
        });
    }, [movieReviewId, createComment, user]);

    const onUpdateComment = useCallback((values) => {
        const updateCommentInput = {
            nodeId: values.nodeId,
            commentPatch: {
                body: values.body,
                title: values.title,
                id: values.id,
                userId: values.userId,
                movieReviewId: movieReviewId,
            }
        };

        return updateComment({
            variables: {
                input: updateCommentInput,
            },
        }).catch(() => {
            errorMessage("Comment updating error.");
        });
    }, [movieReviewId, updateComment]);

    useEffect(() => {
        if (movieReviewComments) {
            setComments(movieReviewComments);
        }
    }, [movieReviewComments]);

    return (
        <div css={movieReviewCommentsContainerStyle} className={className}>
            {loading && (
                <Loading onTop overlay />
            )}
            {comments?.map(comment => (
                <MovieReviewComment
                    key={comment.id}
                    comment={comment}
                    onUpdateComment={onUpdateComment}
                    onDeleteComment={onDeleteComment}
                />
            ))}
            <MovieReviewCreateCommentForm
                movieReviewId={movieReviewId}
                onSubmit={onCreateComment}
            />
        </div>
    )
}

const CREATE_COMMENT_QUERY = gql`
    mutation createComment ($input: CreateCommentInput!) {
        createComment(input: $input) {
            comment {
                nodeId
                id
                title
                body
                userByUserId {
                    id
                    name
                }
            }
        }
    }
`;

const UPDATE_COMMENT_QUERY = gql`
    mutation updateComment ($input: UpdateCommentInput!) {
        updateComment(input: $input) {
            comment {
                nodeId
            }
        }
    }
`;

const DELETE_COMMENT_QUERY = gql`
    mutation deleteComment ($input: DeleteCommentInput!) {
        deleteComment(input: $input) {
            deletedCommentId
        }
    }
`;

const movieReviewCommentsContainerStyle = theme => css`
    display: flex;
    flex-wrap: wrap;
    width: 100%;
    min-height: 400px;
    position: relative;
    border-top: 2px solid ${theme.colours.grey[300]};
    padding-top: 20px;
`;

MovieReviewCommentList.propTypes = {
    movieReviewComments: PropTypes.array,
    loading: PropTypes.bool,
    className: PropTypes.string,
    movieReviewId: PropTypes.string,
}

export default memo(MovieReviewCommentList);