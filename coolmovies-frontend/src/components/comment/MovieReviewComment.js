import React, { memo, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import PropTypes from "prop-types";
import { css } from "@emotion/react";
import { useFormik } from "formik";
import { trim } from "lodash";
import LabelledField from "../form/LabelledField";
import Textarea from "../form/Textarea";
import Button from "../form/Button";
import { NOOP } from "../../core/constants";
import { useCurrentUserContext } from "../../core/contexts/CurrentUserContext";
import CommonLink from "../links/CommonLink";
import ReviewerLink from "../links/ReviewerLink";
import DeletionConfirmDialog from "../dialog/DeletionConfirmDialog";

const MovieReviewComment = ({
    comment,
    className = '',
    onUpdateComment = NOOP,
    onDeleteComment = NOOP,
}) => {
    const [modalIsOpen, setModalIsOpen] = useState(false);
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [isEditMode, setIsEditMode] = useState(false);
    const { user } = useCurrentUserContext();
    const textareaRef = useRef();

    const isEditable = useMemo(() => {
        return user.id === comment.userByUserId.id;
    }, [user, comment]);

    const setRef = useCallback(ref => {
        textareaRef.current = ref;
    }, []);

    const setEditMode = useCallback(() => {
        setIsEditMode(true);
    }, []);

    const onDeleteCommentClicked = useCallback(async () => {
        setIsSubmitting(true);
        await onDeleteComment(comment.nodeId);
        setIsSubmitting(false);
    }, [onDeleteComment, comment]);

    const onCloseModal = useCallback(() => {
        setModalIsOpen(false);
    }, []);

    const onOpenDeletionConfirm = useCallback(() => {
        setModalIsOpen(true);
    }, []);

    useEffect(() => {
        if (isEditMode) {
            textareaRef.current.focus();
        }
    }, [isEditMode]);

    const {
        values,
        handleSubmit,
        handleChange,
        isValid,
        resetForm,
    } = useFormik({
        initialValues: {
            nodeId: comment.nodeId,
            id: comment.id,
            title: comment.title,
            body: comment.body,
            userId: comment.userByUserId.id,
        },
        validateOnMount: true,
        validate,
        onSubmit: async (values) => {
            setIsSubmitting(true);
            await onUpdateComment(values);
            setIsEditMode(false);
            setIsSubmitting(false);
        }
    });

    const cancelEditMode = useCallback(() => {
        resetForm();
        setIsEditMode(false);
    }, [resetForm]);

    return (
        <div className={className} css={style}>
            <form onSubmit={handleSubmit} css={formStyle}>
                <LabelledField name="body" label={<ReviewerLink user={comment.userByUserId} />}>
                    <Textarea
                        innerRef={setRef}
                        name="body"
                        onChange={handleChange}
                        value={values.body}
                        {...isSubmitting && { disabled: true }}
                        {...isEditMode
                            ? {
                                hasBorder: true,
                                autoFocus: true,
                            }
                            : { disabled: true }
                        }
                        autoComplete={"off"}
                        css={textareaContainerStyle}
                    />

                    {isEditable && (
                        isEditMode
                            ? (
                                <div css={buttonsContainerStyle}>
                                    <Button onClick={cancelEditMode} variant={"secondary"} disabled={isSubmitting}>
                                        Cancel
                                    </Button>
                                    <Button disabled={!isValid} loading={isSubmitting}>
                                        Save
                                    </Button>
                                </div>
                            )
                            : (
                                <div css={editButtonStyle}>
                                    <CommonLink onClick={setEditMode}>Edit</CommonLink>
                                    <CommonLink onClick={onOpenDeletionConfirm}>Delete</CommonLink>
                                </div>
                            )
                    )}
                </LabelledField>
            </form>

            {modalIsOpen && (
                <DeletionConfirmDialog
                    onCloseClicked={onCloseModal}
                    onSaveClicked={onDeleteCommentClicked}
                    loading={isSubmitting}
                >
                    Do you want to delete this comment?
                </DeletionConfirmDialog>
            )}
        </div>
    );
};

const validate = values => {
    const errors = {};

    if (!trim(values.body)) {
        errors.body = 'Required';
    }
    return errors;
};

const style = theme => css`
    height: 100%;
    width: 100%;
    border-bottom: 2px solid ${theme.colours.grey[300]};
    margin-bottom: 20px;
`;

const editButtonStyle = css`
    position: absolute;
    bottom: 0;
    right: 0;

    > * {
        margin-right: 10px;
    }
`;

const buttonsContainerStyle = css`
    display: flex;
    flex-direction: row;
    position: absolute;
    bottom: 0;
    right: 0;

    > * {
        margin-left: 10px;
    }
`;

const textareaContainerStyle = css`
    height: 100%;
    min-height: 100%;
    width: 100%;
    margin-bottom: 30px;
`;

const formStyle = css`
    display: flex;
    flex-direction: column;
    margin-bottom: 20px;
    width: 100%;
    position: relative;
`;

MovieReviewComment.propTypes = {
    comment: PropTypes.object.isRequired,
    className: PropTypes.string,
    onUpdateComment: PropTypes.func,
    onDeleteComment: PropTypes.func,
}

export default memo(MovieReviewComment);