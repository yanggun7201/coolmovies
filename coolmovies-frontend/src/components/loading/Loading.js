import React, { useMemo } from "react";
import PropTypes from "prop-types";
import { css, keyframes } from "@emotion/react";
import colours from "../../theme/colours";

const Loading = ({
    onTop = false,
    overlay = false,
    fitToScreen = false,
    onlyOverlay = false,
    overlayColour = "white",
    small = false,
    delay = 0,
    loadingColour = colours.eco,
}) => {
    const loadingStyle = useMemo(() => {
        if (onTop) {
            return css`
                width: 100%;
                position: absolute;
                top: 0;
                left: 0;
                height: 100%;
                display: flex;
                animation: ${delayKeyframe} ${delay}ms;
            `;
        }
        return css`
            width: 100%;
            height: 50px;
            position: relative;
            animation: ${delayKeyframe} ${delay}ms;
        `;
    }, [onTop, delay]);

    const loadingRingStyle = useMemo(() => {
        return theme => css`
            display: inline-block;
            position: relative;
            width: ${small ? 30 : 64}px;
            height: ${small ? 30 : 64}px;
            z-index: ${theme.zIndex.zIndexHigh};
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        `;
    }, [small]);

    const overlayStyle = useMemo(() => {
        if (fitToScreen) {
            return theme => css`
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                opacity: 0.25;
                z-index: ${theme.zIndex.zIndexHigh - 1};
                background-color: ${overlayColour};
            `;
        }

        return theme => css`
            position: absolute;
            width: 100%;
            height: 100%;
            opacity: 0.25;
            z-index: ${theme.zIndex.zIndexHigh - 1};
            background-color: ${overlayColour};
        `;
    }, [fitToScreen, overlayColour]);

    return (
        <div css={loadingStyle}>
            {overlay && (
                <div css={overlayStyle} />
            )}
            {!onlyOverlay && (
                <div css={loadingRingStyle}>
                    <div css={loadingRingSectionStyle(small, loadingColour)} />
                    <div css={loadingRingSectionStyle(small, loadingColour)} />
                    <div css={loadingRingSectionStyle(small, loadingColour)} />
                    <div css={loadingRingSectionStyle(small, loadingColour)} />
                </div>
            )}
        </div>
    );
}

const delayKeyframe = keyframes`
    0% {
        visibility: hidden;
        position: fixed;
    }

    99.9% {
        visibility: hidden;
        position: fixed;
    }

    100% {
        visibility: visible;
        position: inherit;
    }
`;

const ringLoadingKeyframe = keyframes`
    0% {
        transform: rotate(0deg);
    }
    100% {
        transform: rotate(360deg);
    }
`;

const loadingRingSectionStyle = (small = false, loadingColour) => css`
    box-sizing: border-box;
    display: block;
    position: absolute;
    width: 80%;
    height: 80%;
    margin: 9%;
    border: ${small ? 4 : 6}px solid ${loadingColour};
    border-radius: 50%;
    animation: ${ringLoadingKeyframe} 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
    border-color: ${loadingColour} transparent transparent transparent;

    :nth-of-type(N+1) {
        animation-delay: -0.45s;
    }

    :nth-of-type(N+2) {
        animation-delay: -0.3s;
    }

    :nth-of-type(N+3) {
        animation-delay: -0.15s;
    }
`;

Loading.propTypes = {
    overlay: PropTypes.bool,
    onTop: PropTypes.bool,
    fitToScreen: PropTypes.bool,
    onlyOverlay: PropTypes.bool,
    small: PropTypes.bool,
    overlayColour: PropTypes.string,
    loadingColour: PropTypes.string,
    delay: PropTypes.number,
};

export default Loading;
