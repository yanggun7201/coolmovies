import React, { memo } from 'react';
import PropTypes from "prop-types";
import { css } from "@emotion/react";

const EmptyMessage = ({ children }) => (
    <div css={style}>
        {children}
    </div>
);

EmptyMessage.propTypes = {
    children: PropTypes.node.isRequired,
};

const style = theme => css`
    flex-grow: 1;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 16px;
    font-weight: 300;
    font-style: italic;
    text-align: center;
    color: ${theme.colours.doveGray2};
`;

export default memo(EmptyMessage);
