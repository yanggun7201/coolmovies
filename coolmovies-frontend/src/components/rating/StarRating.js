import React, { memo } from "react";
import StarRatings from "react-star-ratings";
import { css } from "@emotion/react";

const StarRating = ({ rating = 0, starDimension = "15px" }) => (
    <div css={style}>
        <StarRatings
            rating={rating / 2}
            starEmptyColor={'transparent'}
            starRatedColor="gold"
            numberOfStars={5}
            name='rating'
            starDimension={starDimension}
            starSpacing={"3px"}
        />
    </div>
);

const style = css`
    display: flex;
`;

export default memo(StarRating);