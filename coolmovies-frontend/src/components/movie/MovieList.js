import React, { memo } from 'react';
import PropTypes from "prop-types";
import { css } from "@emotion/react";
import { isEmpty } from "lodash";
import Loading from "../../components/loading/Loading";
import MovieCard from "./MovieCard";
import EmptyMessage from "../messages/EmptyMessage";

const MovieList = ({
    movies = [],
    loading = false,
    className = '',
}) => (
    <div css={movieContainerStyle} className={className}>
        {loading && (
            <Loading onTop overlay />
        )}
        {!loading && isEmpty(movies) && (
            <EmptyMessage>No movies are available for the current filter.</EmptyMessage>
        )}
        {movies?.map(movie => (
            <MovieCard
                key={movie.id}
                movie={movie}
                css={movieCardStyle}
            />
        ))}
    </div>
);

const movieContainerStyle = css`
    display: flex;
    flex-wrap: wrap;
    width: 100%;
    min-height: 400px;
    position: relative;
`;

const movieCardStyle = theme => css`
    width: 25%;

    ${theme.breakpoints.down("md")} {
        width: 50%;
    }

    ${theme.breakpoints.down("xs")} {
        width: 100%;
        min-width: 300px;
    }
`;

MovieList.propTypes = {
    movies: PropTypes.array,
    loading: PropTypes.bool,
    className: PropTypes.string,
}

export default memo(MovieList);