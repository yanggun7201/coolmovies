import React, { memo } from 'react';
import PropTypes from "prop-types";
import { css } from "@emotion/react";
import Loading from "../../components/loading/Loading";
import MovieReviewCard from "./MovieReviewCard";

const MovieReviewList = ({
    movieReviews = [],
    loading = false,
    className = '',
}) => (
    <div css={movieReviewContainerStyle} className={className}>
        {loading && (
            <Loading onTop overlay />
        )}
        {movieReviews?.map(movieReview => (
            <MovieReviewCard
                key={movieReview.id}
                movieReview={movieReview}
                css={movieReviewCardStyle}
            />
        ))}
    </div>
);

const movieReviewContainerStyle = css`
    display: flex;
    flex-wrap: wrap;
    width: 100%;
    min-height: 400px;
    position: relative;
`;

const movieReviewCardStyle = theme => css`
    width: 25%;

    ${theme.breakpoints.down("md")} {
        width: 50%;
    }

    ${theme.breakpoints.down("xs")} {
        width: 100%;
        min-width: 300px;
    }
`;

MovieReviewList.propTypes = {
    movieReviews: PropTypes.array,
    loading: PropTypes.bool,
    className: PropTypes.string,
}

export default memo(MovieReviewList);