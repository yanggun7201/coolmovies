import React from 'react';
import PropTypes from "prop-types";
import { css } from "@emotion/react";
import { Link } from "react-router-dom";
import StarRating from "../rating/StarRating";
import noImageURL from "../../images/no-image.png";

const MovieReviewCard = ({
    movieReview,
    className = '',
}) => {
    return (
        <div className={className} css={movieReviewCardContainerStyle}>
            <div css={overLayStyle}>
                <Link
                    css={movieReviewLinkStyle}
                    to={`/reviews/${movieReview.id}`}
                >
                    <figure>
                        <img css={imageStyle} src={noImageURL} alt={""}/>
                        <figcaption css={movieReviewInfoStyle}>
                            <h6 css={movieTitleStyle}>
                                {movieReview.movieByMovieId?.title || movieReview.title}
                            </h6>
                            <div css={movieReviewSubHeadingStyle}>
                                <div>
                                    <StarRating rating={movieReview.rating} />
                                </div>
                                <h6 css={authorStyle}>
                                    {movieReview.userByUserReviewerId.name}
                                </h6>
                            </div>
                        </figcaption>
                    </figure>
                </Link>
            </div>
        </div>
    );
};

const movieReviewCardContainerStyle = css`
    display: block;
    padding: .75rem;
    width: 100%;
`;

const overLayStyle = theme => css`
    overflow: hidden;
    border-radius: ${theme.borderAndShadow.smallRadius};
`;

const movieReviewLinkStyle = theme => css`
    display: block;
    position: relative;
    cursor: pointer;
    color: #fff;

    ::before {
        content: '';
        background-color: ${theme.colours.utils.rgba(theme.colours.white, 0)};
        transition: background-color ${theme.transitions.transition};
        will-change: auto;
        width: 100%;
        height: 100%;
        position: absolute;
        pointer-events: none;
    }

    :hover {
        ::before {
            background-color: ${theme.colours.utils.rgba(theme.colours.white, 0.20)};
        }
    }
`;

const imageStyle = css`
    height: auto;
    max-width: 100%;
    width: 100%;
    display: block;
    object-fit: cover;
`;

const movieReviewInfoStyle = css`
    position: absolute;
    top: auto;
    left: 0;
    bottom: 0;
    right: 0;
    padding: .5rem 1rem;
    width: 100%;
    background: linear-gradient(180deg, transparent, #000);
`;

const movieTitleStyle = css`
    font-weight: 700;
    font-style: normal;
    font-size: 16px;
    line-height: 19px;
    color: #fff;
    margin: 0 0 .25rem;
`;

const movieReviewSubHeadingStyle = css`
    display: flex;
    justify-content: space-between;
`;

const authorStyle = css`
    display: flex;
    font-weight: 600;
    font-size: 13px;
    line-height: 16px;
    color: #fff;
    align-items: center;
    margin: 0 0 4px;
`;

MovieReviewCard.propTypes = {
    movieReview: PropTypes.object.isRequired,
    className: PropTypes.string,
}

export default MovieReviewCard;