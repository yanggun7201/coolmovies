import React, { memo, useCallback } from 'react';
import PropTypes from "prop-types";
import { css } from '@emotion/react';
import { NOOP } from "../../core/constants";

const DropdownList = ({
    options = [],
    selected,
    setDropped = NOOP,
    setSelected = NOOP,
}) => {

    const onItemClick = useCallback((e, option) => {
        setSelected(e, option);
        setDropped(false);
    }, [setDropped, setSelected]);

    return (
        <div css={containerStyle}>
            <div css={contentContainerStyle}>
                <div css={itemContainerStyle}>
                    {options.map((option, index) => (
                        <div
                            css={itemStyle(!!selected && option.id === selected.id, index === options.length - 1)}
                            key={index}
                            onClick={(e) => onItemClick(e, option)}
                        >
                            <p>{option.value}</p>
                        </div>
                    ))}
                </div>

            </div>
        </div>

    );
};

const containerStyle = theme => css`
    display: flex;
    border-radius: 3px;
    position: absolute;
    top: 100%;
    left: 0;
    width: 100%;
    z-index: ${theme.zIndex.zIndexTwo};
`;

const contentContainerStyle = css`
    background-color: white;
    position: relative;
    width: 100%;
    margin: auto;
    box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.6);
    border-radius: 3px;
`;

const itemContainerStyle = css`
    max-height: 200px;
    overflow-y: auto;
    overflow-x: hidden;
`;

const itemStyle = (selected, isLast) => theme => css`
    display: flex;
    padding: 5px;
    height: 40px;
    box-sizing: border-box;
    background-color: ${selected && theme.colours.hawkesBlue};
    border-bottom: ${selected && '2px solid' + theme.colours.cornflower};

    &:hover {
        border-radius: ${isLast && '0 0 3px 3px'};
        background-color: ${!selected && theme.colours.solitude};
        cursor: ${selected ? 'default' : 'pointer'}
    }

    p {
        font-size: 100%;
        font-weight: 400;
        margin: auto 0;
        user-select: none;
    }
`;

DropdownList.propTypes = {
    selected: PropTypes.object,
    setSelected: PropTypes.func,
    setDropped: PropTypes.func,
    options: PropTypes.array,
};

export default memo(DropdownList);
