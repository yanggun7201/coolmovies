import React, { memo, useCallback } from 'react';
import PropTypes from "prop-types";
import { css } from '@emotion/react';
import { NOOP } from "../../core/constants";

const Input = ({
    hasError = false,
    id = '',
    innerRef,
    name = '',
    onChange = NOOP,
    value = '',
    label = '',
    hasBorder = false,
    className = '',
    variant = "text",
    ...otherProps
}) => {

    const handleChange = useCallback((e) => {
        if (onChange) {
            onChange(e, name, e.target.value);
        }
    }, [name, onChange]);

    return (
        <div css={containerStyle} className={className}>
            <input
                css={inputStyle(hasError, hasBorder)}
                id={id || name}
                name={name}
                ref={innerRef}
                value={value || ''}
                onChange={handleChange}
                placeholder={label}
                type={variant}
                {...otherProps}
            />
        </div>
    );
};

const containerStyle = theme => css`
    position: relative;
    ${theme.forms.defaults};
`;

const inputStyle = (hasError, hasBorder) => theme => css`
    border: 1px solid ${hasBorder ? theme.colours.border : theme.colours.transparent};
    border-radius: 3px;
    height: 50px;
    padding: 14px;
    background: transparent;
    outline: none;
    width: 100%;
    font-size: 16px;
    font-weight: 400;
    ${hasError && theme.forms.error};
    ${theme.mixins.placeholder()};
    transition: border ${theme.transitions.transition};

    ${hasBorder && css`
        :focus,
        :hover {
            border: 1px solid ${theme.colours.utils.darken(1.0, theme.colours.border)};
        }
    `};
`;

Input.propTypes = {
    hasError: PropTypes.bool,
    id: PropTypes.string,
    innerRef: PropTypes.oneOf([React.RefCallback, React.Ref]),
    name: PropTypes.string,
    label: PropTypes.string,
    onChange: PropTypes.func,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    variant: PropTypes.oneOf(["text", "number", "email"]),
    hasBorder: PropTypes.bool,
    className: PropTypes.string,
}

export default memo(Input);
