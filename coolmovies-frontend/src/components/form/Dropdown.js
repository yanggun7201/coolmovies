import React, { memo, useCallback, useEffect, useRef, useState } from 'react';
import PropTypes from "prop-types";
import { css } from '@emotion/react';
import DropdownChevron from "./DropdownChevron";
import { NOOP } from "../../core/constants";
import DropdownList from "./DropdownList";

const Dropdown = ({
    className = '',
    hasBorder = false,
    hasError = false,
    options = [],
    placeholder = '',
    readOnly = false,
    selected = null,
    setSelected = NOOP,
}) => {
    const [dropped, setDropped] = useState(false);
    const dropdownRef = useRef(null);

    const toggle = useCallback(() => {
        if (readOnly) return;
        setDropped(dropped => !dropped);
    }, [setDropped, readOnly]);

    useEffect(() => {
        const handleClickOutside = event => {
            if (dropdownRef.current && !dropdownRef.current.contains(event.target)) {
                setDropped(false);
            }
        }

        document.addEventListener("mousedown", handleClickOutside);

        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, [dropdownRef]);

    return (
        <div css={containerStyle} ref={dropdownRef} className={className}>
            <div css={selectedContainerStyle(hasBorder, hasError, readOnly)} onClick={toggle}>
                {selected
                    ? (<div css={selectedItemStyle}>{selected.value}</div>)
                    : (<div css={placeholderStyle}>{placeholder && placeholder}</div>)
                }
                {!readOnly && (
                    <DropdownChevron
                        dropped={dropped}
                        hasError={hasError}
                        css={dropdownChevronStyle}
                    />
                )}
            </div>
            {dropped && (
                <DropdownList
                    selected={selected}
                    setSelected={setSelected}
                    setDropped={setDropped}
                    options={options}
                />
            )}
        </div>
    );
};

const containerStyle = css`
    display: flex;
    flex-direction: row;
    position: relative;
    width: 100%;
    height: 50px;
`;

const selectedContainerStyle = (hasBorder, hasError, readOnly) => theme => css`
    flex-grow: 1;
    display: flex;
    position: relative;
    border: 1px solid ${hasBorder ? theme.colours.border : theme.colours.transparent};
    border-radius: 3px;
    justify-content: space-between;
    ${hasError && theme.forms.error};
    transition: border ${theme.transitions.transition};

    ${!readOnly && css`
        :hover {
            cursor: pointer;
        }
    `};

    ${hasBorder && css`
        :focus,
        :hover {
            border: 1px solid ${theme.colours.utils.darken(1.0, theme.colours.border)};
        }
    `};
`;

const selectedItemStyle = css`
    margin: auto 0 auto 10px;
    font-size: 100%;
`;

const placeholderStyle = theme => css`
    margin: auto 0 auto 10px;
    font-size: 100%;

    ${theme.mixins.placeholderAttributes()};
`;

const dropdownChevronStyle = css`
    right: 15px;
`;

Dropdown.propTypes = {
    className: PropTypes.string,
    hasBorder: PropTypes.bool,
    hasError: PropTypes.bool,
    options: PropTypes.array,
    placeholder: PropTypes.string,
    readOnly: PropTypes.bool,
    selected: PropTypes.object,
    setSelected: PropTypes.func,
};

export default memo(Dropdown);
