import React, { memo, useCallback } from 'react';
import PropTypes from "prop-types";
import { css } from '@emotion/react';
import AutoSizeTextarea from "react-textarea-autosize";
import { NOOP } from "../../core/constants";
import { extraCss, extraCssPropType } from "../../core/includes/emotion";

const Textarea = ({
    hasError = false,
    id = '',
    innerRef,
    name = '',
    onChange = NOOP,
    value = '',
    label = '',
    hasBorder = false,
    autoFocus = false,
    disabled = false,
    className = '',
    extraTextareaStyle = NOOP,
    ...otherProps
}) => {

    const handleChange = useCallback((e) => {
        if (onChange) {
            onChange(e, name, e.target.value);
        }
    }, [name, onChange]);

    return (
        <div css={containerStyle} className={className}>
            <AutoSizeTextarea
                css={inputStyle(hasError, hasBorder, extraTextareaStyle)}
                {...id && { id }}
                name={name}
                minRows={2}
                ref={innerRef}
                value={value || ''}
                onChange={handleChange}
                placeholder={label}
                autoFocus={autoFocus}
                disabled={disabled}
                {...otherProps}
            />
        </div>
    );
};

const containerStyle = theme => css`
    position: relative;
    ${theme.forms.defaults};
`;

const inputStyle = (hasError, hasBorder, extraTextareaStyle) => theme => css`
    border: 1px solid ${hasBorder ? theme.colours.border : theme.colours.transparent};
    border-radius: 3px;
    min-height: 50px;
    padding: 14px;
    background: transparent;
    outline: none;
    width: 100%;
    height: 100%;
    font-size: 16px;
    font-weight: 400;
    ${hasError && theme.forms.error};
    ${theme.mixins.placeholder()};
    transition: border ${theme.transitions.transition};
    resize: none;

    ${hasBorder && css`
        :focus,
        :hover {
            border: 1px solid ${theme.colours.utils.darken(1.0, theme.colours.border)};
        }
    `};

    ${extraCss(extraTextareaStyle, theme)};
`;

Textarea.propTypes = {
    hasError: PropTypes.bool,
    id: PropTypes.string,
    innerRef: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
    name: PropTypes.string,
    label: PropTypes.string,
    onChange: PropTypes.func,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    hasBorder: PropTypes.bool,
    autoFocus: PropTypes.bool,
    disabled: PropTypes.bool,
    className: PropTypes.string,
    extraTextareaStyle: extraCssPropType,
}

export default memo(Textarea);
