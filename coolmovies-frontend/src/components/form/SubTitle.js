import React, { memo } from 'react';
import PropTypes from "prop-types";
import { css } from '@emotion/react';

const SubTitle = ({ children, className = '' }) => (
    <h1 css={titleStyle} className={className}>{children}</h1>
);

const titleStyle = theme => css`
    font-weight: 700;
    font-style: normal;
    font-size: 24px;
    line-height: 28px;

    ${theme.breakpoints.down("md")} {
        font-weight: 700;
        font-style: normal;
        font-size: 22px;
        line-height: 26px;
    }

    ${theme.breakpoints.down("sm")} {
        font-weight: 500;
        font-style: normal;
        font-size: 20px;
        line-height: 24px;
    }
`;

SubTitle.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
}

export default memo(SubTitle);
