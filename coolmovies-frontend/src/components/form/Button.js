import React, { useEffect, useMemo, useRef, useState } from 'react';
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { css, useTheme } from "@emotion/react";
import Loading from "../loading/Loading";

const Button = ({
    className = '',
    to = '',
    disabled = false,
    loading = false,
    children = '',
    variant = 'primary',
    onClick,
    type = "submit",
}) => {
    const [isLoading, setIsLoading] = useState(false);
    const unmounted = useRef(false);
    const theme = useTheme();

    useEffect(() => {
        return () => {
            unmounted.current = true;
        }
    }, [])

    const onClickFunc = e => {
        if (isLoading) return;
        const clickResult = onClick(e);
        if (clickResult instanceof Promise) {
            setIsLoading(true);

            clickResult.finally(() => {
                if (!unmounted.current) setIsLoading(false);
            });
        }
    };

    const Tag = useMemo(() => {
        if (to) {
            return Link;
        }
        return "button";
    }, [to]);

    return (
        <Tag
            to={to}
            type={type}
            className={className}
            css={buttonStyle(variant, disabled, (isLoading || loading))}
            {...(disabled || isLoading || loading) && { disabled: "disabled" }}
            {...onClick && { onClick: onClickFunc }}
        >
            {(isLoading || loading) && (
                <Loading overlay={false} onTop small loadingColour={getLoadingColour(variant, theme)} />
            )}
            {children}
        </Tag>
    );
}

const getLoadingColour = (variant, theme) => {
    switch (variant) {
        case 'cancel':
            return theme.colours.red[350];
        case 'primary':
        case 'secondary':
        default:
            return theme.colours.white;
    }
};

const buttonColourStyle = (variant, theme, isLoading, disabled) => css`
    ${variant === 'primary' && css`
        background: ${theme.colours.eco};
        border: 0;
        color: ${theme.colours.white};

        ${!isLoading && css`
            &:hover {
                background-color: ${theme.colours.utils.darken(0.1, theme.colours.eco)};
                color: ${theme.colours.white};
            }

            &:focus {
                box-shadow: 0 0 0 .2rem ${theme.colours.utils.rgba(theme.colours.eco, 0.7)};
            }
        `}
    `};

    ${variant === 'secondary' && css`
        background: ${theme.colours.grey[200]};
        border: 0;
        color: ${theme.colours.text};

        ${!isLoading && css`
            &:hover {
                background-color: ${theme.colours.utils.darken(0.1, theme.colours.grey[200])};
                color: ${theme.colours.text};
            }

            &:focus {
                box-shadow: 0 0 0 .2rem ${theme.colours.utils.rgba(theme.colours.grey[200], 0.7)};
            }
        `}
    `};

    ${variant === 'cancel' && css`
        background: transparent;
        border: 1px solid ${theme.colours.red[350]};
        color: ${theme.colours.red[350]};

        ${!isLoading && css`
            &:hover {
                background: ${theme.colours.red[350]};
                color: ${theme.colours.white};
            }

            &:focus {
                box-shadow: 0 0 0 .2rem ${theme.colours.utils.rgba(theme.colours.red[350], 0.7)};
            }
        `}
    `};
`;

const buttonStyle = (variant, disabled, isLoading) => theme => css`
    position: relative;
    display: inline-flex;
    min-width: 80px;
    width: max-content;
    padding: 0 16px;
    line-height: 33px;
    height: 32px;
    justify-content: center;
    align-items: center;
    font-size: 16px;
    border-radius: 3px;
    text-decoration: none;
    transition: background-color ${theme.transitions.transitionFaster}, box-shadow ${theme.transitions.transitionFaster};

    :active,
    :visited {
        color: ${theme.colours.white};
    }

    ${!disabled && css`
        cursor: pointer;
    `}

    ${(disabled || isLoading) && css`
        opacity: .5;
    `}

    ${buttonColourStyle(variant, theme, isLoading || disabled)};
`;

Button.propTypes = {
    className: PropTypes.string,
    to: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    disabled: PropTypes.bool,
    loading: PropTypes.bool,
    children: PropTypes.node,
    onClick: PropTypes.func,
    variant: PropTypes.oneOf(["primary", "cancel", "secondary"]),
}

export default Button;