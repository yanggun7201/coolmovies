import React, { memo } from 'react';
import PropTypes from "prop-types";
import { css } from '@emotion/react';

const LabelledField = ({
    className,
    children,
    error,
    label,
    name,
}) => {
    return (
        <div className={className} css={containerStyle}>
            <label css={labelStyle} htmlFor={name}>
                {label}
            </label>
            {children}
            {error && (
                <label css={errorStyle} htmlFor={name}>{error}</label>
            )}
        </div>
    );
};

const containerStyle = css`
    position: relative;
    margin-bottom: 19px;

    &:last-child {
        margin-bottom: 0;
    }
`;

const errorStyle = theme => css`
    position: absolute;
    top: 100%;
    font-size: 12px;
    color: ${theme.colours.crimson};
    white-space: nowrap;
`;

const labelStyle = theme => css`
    font-size: 16px;
    color: ${theme.colours.text};
    display: block;
    padding: 0;
`;

LabelledField.proptTypes = {
    children: PropTypes.node.isRequired,
    error: PropTypes.string,
    label: PropTypes.node.isRequired,
    name: PropTypes.string.isRequired,
    className: PropTypes.string,
};

export default memo(LabelledField);
