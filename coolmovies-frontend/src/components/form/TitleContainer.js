import React from "react";
import { css } from "@emotion/react";

const TitleContainer = ({
    children,
    className = '',
}) => (
    <div css={titleContainerStyle} className={className}>
        {children}
    </div>
);

const titleContainerStyle = theme => css`
    margin-bottom: 30px;

    ${theme.breakpoints.down("md")} {
        margin-bottom: 20px;
    }

    ${theme.breakpoints.down("sm")} {
        margin-bottom: 10px;
    }
`;

export default TitleContainer;