import React, { memo } from 'react';
import { css } from '@emotion/react';
import chevron from "../../images/chevron.svg";
import { NOOP } from "../../core/constants";

const DropdownChevron = ({
    dropped = false,
    className = '',
    hasError = false,
    onClick = NOOP,
}) => (
    <div
        css={style(dropped, hasError)}
        className={className}
        onClick={onClick}
    />
)

const style = (dropped, hasError) => theme => css`
    position: absolute;
    top: 52%;
    right: 0;
    transform: translateY(-50%);
    mask: url(${chevron}) no-repeat center/11px;
    background-color: ${hasError ? theme.colours.crimson : theme.colours.grey[920]};
    width: 20px;
    height: 20px;

    ${dropped && css`
        transform: translateY(-50%) scaleY(-1);
    `}
`;

export default memo(DropdownChevron);
