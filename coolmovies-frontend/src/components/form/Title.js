import React, { memo } from 'react';
import PropTypes from "prop-types";
import { css } from '@emotion/react';

const Title = ({ children, className = '' }) => (
    <h1 css={titleStyle} className={className}>{children}</h1>
);

const titleStyle = theme => css`
    font-weight: 700;
    font-style: normal;
    font-size: 40px;
    line-height: 44px;

    ${theme.breakpoints.down("md")} {
        font-weight: 700;
        font-style: normal;
        font-size: 32px;
        line-height: 36px;
    }

    ${theme.breakpoints.down("sm")} {
        font-weight: 500;
        font-style: normal;
        font-size: 26px;
        line-height: 30px;
    }
`;

Title.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
}

export default memo(Title);
