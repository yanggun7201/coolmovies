import React, { memo } from 'react';
import PropTypes from "prop-types";
import { css } from "@emotion/react";
import CommonLink from "./CommonLink";

const LinkAboveTitle = ({
    children,
    className,
    to = '#',
    onClick = null
}) => (
    <CommonLink
        to={to}
        css={linkAboveTitleStyle}
        onClick={onClick}
        className={className}
    >
        {children}
    </CommonLink>
);

const linkAboveTitleStyle = theme => css`
    ${theme.breakpoints.down("xs")} {
        visibility: hidden;
    }
`;

LinkAboveTitle.propTypes = {
    children: PropTypes.node.isRequired,
    to: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    className: PropTypes.string,
    onClick: PropTypes.func,
}

export default memo(LinkAboveTitle);