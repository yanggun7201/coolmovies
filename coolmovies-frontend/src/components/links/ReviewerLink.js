import React from "react";
import PropTypes from "prop-types";
import { css } from "@emotion/react";
import CommonLink from "./CommonLink";

const ReviewerLink = ({ user, className = '' }) => (
    <CommonLink to={`/reviewers/${user.id}`} css={style} className={className}>
        {user.name}
    </CommonLink>
);

ReviewerLink.propTypes = {
    user: PropTypes.object.isRequired,
    className: PropTypes.string,
}

const style = css`
    font-size: 16px;
    font-weight: 500;
`;

export default ReviewerLink;