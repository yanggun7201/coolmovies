import React, { memo } from 'react';
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { css } from "@emotion/react";

const CommonLink = ({
    children,
    className,
    to = '#',
    onClick = null
}) => (
    <Link
        {...to && { to }}
        {...onClick && { onClick }}
        css={style}
        className={className}
    >
        {children}
    </Link>
);

const style = theme => css`
    ${theme.mixins.animatedUnderlineItSelf()};
`;

CommonLink.propTypes = {
    children: PropTypes.node.isRequired,
    to: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    className: PropTypes.string,
    onClick: PropTypes.func,
}

export default memo(CommonLink);