import React, { memo } from 'react';
import PropTypes from "prop-types";
import { css } from "@emotion/react";

const DialogActions = ({
    align = 'right',
    children,
    className = '',
}) => (
    <div css={style(align)} className={className}>{children}</div>
);

const style = align => css`
    padding-top: 20px;
    text-align: ${align};

    > button {
        margin-left: 5px;
    }
`;

DialogActions.propTypes = {
    children: PropTypes.node,
    align: PropTypes.oneOf(["center", "left", "right"]),
    className: PropTypes.string,
};

export default memo(DialogActions);
