import React, { memo, useCallback, useEffect } from 'react';
import PropTypes from "prop-types";
import { css } from "@emotion/react";
import FocusLock from 'react-focus-lock';
import { NOOP } from "../../core/constants";
import { enableBodyScrolling, isMobile, preventBodyScrolling } from "../../core/includes/domUtils";

const Dialog = ({
    children,
    onClose = NOOP,
    onOverlayClick = NOOP,
    overlay = true,
    showCloseButton = true,
    size,
}) => {

    useEffect(() => {
        if (isMobile()) {
            preventBodyScrolling();
        }

        return () => {
            if (isMobile) {
                enableBodyScrolling();
            }
        };
    }, []);

    const handleClose = useCallback(e => {
        e.preventDefault();
        e.stopPropagation();

        if (onClose !== NOOP) {
            onClose(e);
        } else if (onOverlayClick !== NOOP) {
            onOverlayClick(e);
        }
    }, [onClose, onOverlayClick]);

    const closeText = size === 'max' ? 'CLOSE' : 'x';

    return (
        <FocusLock>
            {overlay && (<div css={overlayStyles} onClick={onOverlayClick} />)}
            <div css={containerStyles(size)} tabIndex={0}>
                {showCloseButton && <span css={closeStyles} onClick={handleClose}>{closeText}</span>}
                {children}
            </div>
        </FocusLock>
    );
};

const overlayStyles = css`
    background-color: rgba(0, 0, 0, 0.5);
    position: fixed;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    z-index: 100;
`;

const getScreenWidth = size => {
    switch (size) {
        case "medium":
        case "max":
            return "500px";
        case "small":
        default:
            return "280px";
    }
}

const containerStyles = size => theme => css`
    width: ${getScreenWidth(size)};
    max-width: 80%;
    max-height: 80%;
    position: fixed;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    z-index: 101;
    padding: 24px;
    background-color: ${theme.colours.white};

    @media (max-width: 616px) {
        padding: 15px;
    }
`;

const closeStyles = theme => css`
    position: absolute;
    right: 0;
    top: 0;
    border-width: 0 0 1px 1px;
    border-color: ${theme.colours.grey[700]};
    border-style: solid;
    font-size: 16px;
    cursor: pointer;
    color: ${theme.colours.white};
    line-height: 20px;
    vertical-align: middle;
    text-align: center;
    display: block;
    width: 24px;
    height: 24px;
    background: ${theme.colours.grey[700]};

    &:hover {
        border-color: ${theme.colours.grey[500]};
        background: ${theme.colours.grey[500]};
    }
`;

Dialog.propTypes = {
    children: PropTypes.node,
    onClose: PropTypes.func,
    onOverlayClick: PropTypes.func,
    overlay: PropTypes.bool,
    size: PropTypes.oneOf(['small', 'medium', 'max']),
    showCloseButton: PropTypes.bool,
};

export default memo(Dialog);
