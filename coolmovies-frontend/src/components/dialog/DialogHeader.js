import React, { memo } from 'react';
import PropTypes from "prop-types";
import { css } from "@emotion/react";

const DialogHeader = ({ children }) => (
    <h2 css={styles}>{children}</h2>
);

const styles = theme => css`
    font-size: 20px;
    font-weight: 400;
    color: ${theme.colours.grey[900]};
    margin-bottom: 12px;
`;

DialogHeader.propTypes = {
    children: PropTypes.node,
};

export default memo(DialogHeader);
