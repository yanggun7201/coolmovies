import React, { memo } from "react";
import PropTypes from "prop-types";
import Dialog from "./Dialog";
import Loading from "../loading/Loading";
import DialogHeader from "./DialogHeader";
import DialogActions from "./DialogActions";
import Button from "../form/Button";
import { NOOP } from "../../core/constants";

const DeletionConfirmDialog = ({
    onCloseClicked = NOOP,
    loading = false,
    onSaveClicked = NOOP,
    children,
}) => (
    <Dialog
        isOpen={true}
        onClose={onCloseClicked}
        showCloseButton={false}
    >
        {loading && <Loading onTop overlay onlyOverlay />}
        <DialogHeader>
            {children}
        </DialogHeader>
        <DialogActions>
            <Button variant={"secondary"} onClick={onCloseClicked}>
                Cancel
            </Button>
            <Button variant={"cancel"} onClick={onSaveClicked}>
                Delete
            </Button>
        </DialogActions>
    </Dialog>
);

DeletionConfirmDialog.propTypes = {
    onCloseClicked: PropTypes.func,
    onSaveClicked: PropTypes.func,
    loading: PropTypes.bool,
    children: PropTypes.node,
}

export default memo(DeletionConfirmDialog);