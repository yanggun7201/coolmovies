import React, { memo } from 'react';
import PropTypes from "prop-types";
import { css } from "@emotion/react";

const DialogContent = ({
    center = false,
    children,
    className
}) => (
    <div css={style(center)} className={className}>{children}</div>
);

const style = center => theme => css`
    text-align: ${center ? 'center' : 'left'};
    font-size: 16px;
    line-height: 24px;
    color: ${theme.colours.doveGray2},
`;

DialogContent.propTypes = {
    children: PropTypes.node,
    center: PropTypes.bool,
    className: PropTypes.string,
};

export default memo(DialogContent);
