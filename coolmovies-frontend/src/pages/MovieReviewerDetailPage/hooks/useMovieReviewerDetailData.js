import { useEffect } from "react";
import { gql, useLazyQuery } from "@apollo/client";
import useSetState from "../../../core/hooks/useSetState";

const DEFAULT_STATE = {
    reviewer: null,
    loading: true,
};

const useMovieReviewerDetailData = (reviewerId) => {
    const [state, setState] = useSetState(DEFAULT_STATE);
    const [getReviewerAndReviews, { data }] = useLazyQuery(GET_REVIEWER_AND_REVIEWS_QUERY);

    useEffect(() => {
        getReviewerAndReviews({
            variables: {
                reviewerId
            }
        });
    }, [getReviewerAndReviews, reviewerId]);

    useEffect(() => {
        if (data) {
            setState({
                reviewer: {
                    ...data.userById,
                    movieReviews: [
                        ...data.allMovieReviews?.nodes ?? [],
                    ]
                },
                loading: false
            });
        }
    }, [data, setState]);

    return state;
}

const GET_REVIEWER_AND_REVIEWS_QUERY = gql`
    query getReviewerAndReviews ($reviewerId: UUID!) { 
        userById(id: $reviewerId) {
            id
            name
        }
        
        allMovieReviews(filter: {userReviewerId: {equalTo: $reviewerId}}) {
            nodes {
                id
                title
                body
                rating
                movieByMovieId {
                    id
                    title
                    releaseDate
                    userByUserCreatorId {
                        id
                        name
                    }
                    movieDirectorByMovieDirectorId {
                        age
                        id
                        name
                    }
                }
                userByUserReviewerId {
                    name
                    id
                }
            }
        }
    }
`;

export default useMovieReviewerDetailData;