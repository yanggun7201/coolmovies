import React from "react";
import { Redirect, useParams } from "react-router-dom";
import useMovieReviewerDetailData from "./hooks/useMovieReviewerDetailData";
import Loading from "../../components/loading/Loading";
import { css } from "@emotion/react";
import MovieReviewList from "../../components/moviereview/MovieReviewList";
import Title from "../../components/form/Title";
import SubTitle from "../../components/form/SubTitle";
import TitleContainer from "../../components/form/TitleContainer";
import LinkAboveTitle from "../../components/links/LinkAboveTitle";

const MovieReviewerDetailPage = () => {
    const { id } = useParams();
    const { loading, reviewer } = useMovieReviewerDetailData(id);

    if (!id || (!loading && !reviewer)) {
        return <Redirect to={"/"} />;
    }

    return (
        <div css={style}>
            {loading
                ? (<Loading onTop overlay />)
                : (
                    <>
                        <TitleContainer>
                            <LinkAboveTitle to={"/reviews"}>Reviews</LinkAboveTitle>
                            <Title>{reviewer.name}</Title>
                        </TitleContainer>

                        <SubTitle>Reviews</SubTitle>
                        <MovieReviewList
                            movieReviews={reviewer.movieReviews}
                            loading={loading}
                        />
                    </>
                )
            }
        </div>
    );
}

const style = css`
    display: flex;
    justify-content: center;
    flex-direction: column;
    width: 100%;
    margin: 0 auto 30px;
    padding: 0 12px;
`;

export default MovieReviewerDetailPage;