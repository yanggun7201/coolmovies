import React from "react";
import { Redirect, useParams } from "react-router-dom";
import useMovieDirectorDetailData from "./hooks/useMovieDirectorDetailData";
import Loading from "../../components/loading/Loading";
import { css } from "@emotion/react";
import MovieList from "../../components/movie/MovieList";
import Title from "../../components/form/Title";
import TitleContainer from "../../components/form/TitleContainer";

const MovieDirectorDetailPage = () => {
    const { id } = useParams();
    const { loading, director } = useMovieDirectorDetailData(id);

    if (!id || (!loading && !director)) {
        return <Redirect to={"/"} />;
    }

    return (
        <div css={style}>
            {loading
                ? (<Loading onTop overlay />)
                : (
                    <>
                        <TitleContainer>
                            <Title>{director.name}</Title>
                        </TitleContainer>
                        <MovieList
                            movies={director?.movies}
                            loading={loading}
                        />
                    </>
                )
            }
        </div>
    );
}

const style = css`
    display: flex;
    justify-content: center;
    flex-direction: column;
    width: 100%;
    margin: 0 auto 30px;
    padding: 0 12px;
`;

export default MovieDirectorDetailPage;