import { useEffect } from "react";
import { gql, useLazyQuery } from "@apollo/client";
import useSetState from "../../../core/hooks/useSetState";

const DEFAULT_STATE = {
    director: null,
    loading: true,
};

const useMovieDirectorDetailData = (movieDirectorId) => {
    const [state, setState] = useSetState(DEFAULT_STATE);
    const [getDirectorAndMovies, { data }] = useLazyQuery(GET_DIRECTOR_QUERY);

    useEffect(() => {
        getDirectorAndMovies({
            variables: {
                movieDirectorId,
            }
        });
    }, [getDirectorAndMovies, movieDirectorId]);

    useEffect(() => {
        if (data) {
            setState({
                director: {
                    ...data.movieDirectorById,
                    movies : [
                        ...data.allMovies?.nodes ?? []
                    ]
                },
                loading: false
            });
        }
    }, [data, setState]);

    return state;
}

const GET_DIRECTOR_QUERY = gql`
    query movieDirectorById ($movieDirectorId: UUID!) { 
        movieDirectorById(id: $movieDirectorId) {
            id
            name
        }
        allMovies ( filter: {movieDirectorId: {equalTo: $movieDirectorId}}) {
            nodes {
                id
                title
                releaseDate
                movieDirectorByMovieDirectorId {
                    id
                    name
                }
            }
            pageInfo {
                hasNextPage
            }
        }
    }
`;

export default useMovieDirectorDetailData;