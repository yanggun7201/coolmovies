import React, { useEffect, useMemo } from "react";
import { Redirect, useHistory, useParams } from "react-router-dom";
import { gql, useMutation } from "@apollo/client";
import useMovieReviewDetailData from "../MovieReviewDetailPage/hooks/useMovieReviewDetailData";
import MovieReviewCreateUpdateForm from "./components/MovieReviewCreateUpdateForm";

const MovieReviewUpdateFormPage = () => {
    const { reviewId } = useParams();
    const history = useHistory();
    const { movieReview, loading } = useMovieReviewDetailData(reviewId);
    const [updateMovieReview, { data }] = useMutation(UPDATE_MOVIE_REVIEW_QUERY);

    const movie = useMemo(() => {
        if (movieReview) {
            return movieReview.movieByMovieId;
        }
    }, [movieReview]);

    useEffect(() => {
        if (data) {
            history.push(`/reviews/${reviewId}`);
        }
    }, [data, reviewId, history]);

    if (!reviewId || (!loading && !movieReview)) {
        return <Redirect to={"/"} />;
    }

    return (
        <MovieReviewCreateUpdateForm
            movieReviewData={movieReview}
            movie={movie}
            createUpdateMovieReviewMutation={updateMovieReview}
            loading={loading}
        />
    );
}

const UPDATE_MOVIE_REVIEW_QUERY = gql`
    mutation updateMovieReview ($input: UpdateMovieReviewInput!) {
        updateMovieReview(input: $input) {
            movieByMovieId {
                id
            }
            movieReview {
                id
                title
                body
                rating
                userByUserReviewerId {
                    name
                }
            }
        }
    }
`;

export default MovieReviewUpdateFormPage;