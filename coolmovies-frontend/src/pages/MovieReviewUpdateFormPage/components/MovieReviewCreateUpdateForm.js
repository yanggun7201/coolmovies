import React, { useCallback, useEffect, useMemo, useState } from "react";
import { trim } from "lodash";
import { css } from "@emotion/react";
import { useFormik } from "formik";

import { RATING_OPTIONS } from "../../../core/constants";
import { useCurrentUserContext } from "../../../core/contexts/CurrentUserContext";

import Loading from "../../../components/loading/Loading";
import CommonLink from "../../../components/links/CommonLink";
import LabelledField from "../../../components/form/LabelledField";
import Input from "../../../components/form/Input";
import DropDown from "../../../components/form/Dropdown";
import Textarea from "../../../components/form/Textarea";
import useToast from "../../../core/hooks/useToast";
import Button from "../../../components/form/Button";
import Title from "../../../components/form/Title";
import ConfirmDialog from "../../../components/dialog/ConfirmDialog";
import TitleContainer from "../../../components/form/TitleContainer";
import { GET_ALL_MOVIE_REVIEWS_QUERY } from "../../MovieReviewsPage/hooks/useMovieReviewsData";

const MovieReviewCreateUpdateForm = ({
    movieReviewData = {},
    movie = {},
    createUpdateMovieReviewMutation,
    loading = false,
}) => {
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [modalIsOpen, setModalIsOpen] = useState(false);
    const [, errorMessage] = useToast();
    const { user } = useCurrentUserContext();

    const isUpdate = useMemo(() => {
        return !!movieReviewData?.nodeId;
    }, [movieReviewData]);

    const onCloseModal = useCallback(() => {
        setModalIsOpen(false);
    }, []);

    const onOpenConfirm = useCallback(() => {
        setModalIsOpen(true);
    }, []);

    const onSubmit = useCallback((values) => {
        const movieReviewInput = {
            ...isUpdate && { nodeId: movieReviewData?.nodeId },
            [isUpdate ? 'movieReviewPatch' : 'movieReview']: {
                title: values.title,
                body: values.body,
                rating: values.rating.id,
                movieId: movie.id,
                userReviewerId: user.id,
            }
        }

        setIsSubmitting(true);

        createUpdateMovieReviewMutation({
            variables: {
                input: movieReviewInput,
            },
            ...(!isUpdate && {
                awaitRefetchQueries: true,
                refetchQueries: [{ query: GET_ALL_MOVIE_REVIEWS_QUERY }],
            })
        }).catch(() => {
            if (isUpdate) {
                errorMessage("Review updating error.");
            } else {
                errorMessage("Review creation error.");
            }
            setIsSubmitting(false);
        });
    }, [isUpdate, movie, movieReviewData, createUpdateMovieReviewMutation, user, errorMessage]);

    const {
        values,
        setValues,
        setFieldValue,
        handleSubmit,
        handleChange,
        isValid
    } = useFormik({
        initialValues: {
            title: '',
            body: '',
            rating: null,
        },
        validateOnMount: true,
        validate,
        onSubmit: values => {
            onOpenConfirm(values);
        },
    });

    const onSubmitConfirmed = useCallback(() => {
        onSubmit(values);
    }, [values, onSubmit]);

    const onRatingChanged = useCallback((e, selectedItem) => {
        setFieldValue("rating", selectedItem);
    }, [setFieldValue]);

    useEffect(() => {
        if (isUpdate) {
            setValues({
                title: movieReviewData.title,
                body: movieReviewData.body,
                rating: RATING_OPTIONS.find(option => option.id === movieReviewData.rating),
            })
        }
    }, [movieReviewData, setValues, isUpdate]);

    return (
        <div css={style}>
            {loading
                ? (<Loading onTop overlay />)
                : (
                    <div css={detailHeaderStyle}>
                        {isUpdate
                            ? (<CommonLink to={`/reviews/${movieReviewData.id}`}>Back to review</CommonLink>)
                            : (<CommonLink to={`/movies/${movie.id}`}>Back to movie</CommonLink>)
                        }

                        <TitleContainer>
                            <Title>{movie?.title}</Title>
                        </TitleContainer>

                        <form onSubmit={handleSubmit} css={formStyle}>
                            <LabelledField name="title" label="Title">
                                <Input
                                    name="title"
                                    onChange={handleChange}
                                    value={values.title}
                                    hasBorder
                                    autoComplete={"off"}
                                    autoFocus
                                />
                            </LabelledField>
                            <LabelledField name="body" label="Content">
                                <Textarea
                                    name="body"
                                    onChange={handleChange}
                                    value={values.body}
                                    hasBorder
                                    autoComplete={"off"}
                                />
                            </LabelledField>
                            <LabelledField
                                name="rating"
                                label="Rating"
                            >
                                <DropDown
                                    selected={values.rating}
                                    setSelected={onRatingChanged}
                                    options={RATING_OPTIONS}
                                    hasBorder
                                />
                            </LabelledField>
                            <Button
                                disabled={!isValid}
                                type="submit"
                                loading={isSubmitting}
                            >
                                {isUpdate ? 'Update review' : 'Create review'}
                            </Button>
                        </form>
                    </div>
                )
            }

            {modalIsOpen && (
                <ConfirmDialog
                    onCloseClicked={onCloseModal}
                    onSaveClicked={onSubmitConfirmed}
                    loading={isSubmitting}
                >
                    {isUpdate
                        ? 'Do you want to update this review?'
                        : 'Do you want to save this review?'
                    }
                </ConfirmDialog>
            )}
        </div>
    );
}

const validate = values => {
    const errors = {};
    if (!trim(values.title)) {
        errors.title = 'Required';
    }

    if (!trim(values.body)) {
        errors.body = 'Required';
    }

    if (!values.rating) {
        errors.rating = 'Required';
    }

    return errors;
};

const style = css`
    display: flex;
    justify-content: center;
    flex-direction: column;
    width: 100%;
    min-width: 300px;
    max-width: 600px;
    margin: 0 auto 30px;
    padding: 0 12px;
`;

const detailHeaderStyle = css`
    margin-bottom: 30px;
`;

const formStyle = css`
    display: flex;
    flex-direction: column;
    margin-bottom: 20px;
`;

export default MovieReviewCreateUpdateForm;