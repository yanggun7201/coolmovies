import { gql, useLazyQuery } from "@apollo/client";
import { useEffect, useMemo } from "react";
import { compareDate, compareNumber, compareText, createSortableItem } from "../../../core/includes/sort";
import useFilters from "../../../core/hooks/useFilters";

export const REVIEW_ORDER_BY_OPTIONS = Object.freeze([
    Object.freeze(createSortableItem({
        id: "title",
        value: "Title",
        sortField: "movieByMovieId.title",
        defaultValue: "",
        sortDirection: "asc",
        compare: compareText,
    })),
    Object.freeze(createSortableItem({
        id: "released_newest",
        value: "Newest by release date",
        sortField: "movieByMovieId.releaseDate",
        defaultValue: "1970-01-01",
        sortDirection: "desc",
        compare: compareDate,
    })),
    Object.freeze(createSortableItem({
        id: "released_oldest",
        value: "Oldest by release date",
        sortField: "movieByMovieId.releaseDate",
        defaultValue: "1970-01-01",
        sortDirection: "asc",
        compare: compareDate,
    })),
    Object.freeze(createSortableItem({
        id: "highest_rated",
        value: "Highest rated",
        sortField: "rating",
        defaultValue: -1,
        sortDirection: "desc",
        compare: compareNumber,
    })),
    Object.freeze(createSortableItem({
        id: "lowest_rated",
        value: "Lowest rated",
        sortField: "rating",
        defaultValue: -1,
        sortDirection: "asc",
        compare: compareNumber,
    })),
]);

const useMovieReviewsData = () => {
    const [getAllMovieReviews, { data }] = useLazyQuery(GET_ALL_MOVIE_REVIEWS_QUERY);

    const movieReviews = useMemo(() => {
        if (data) {
            return data?.allMovieReviews?.nodes?.slice();
        }
        return [];
    }, [data]);

    const filterState = useFilters(movieReviews, REVIEW_ORDER_BY_OPTIONS, {
        getTitle: movieReview => movieReview.movieByMovieId.title,
        getDirector: movie => movie?.movieByMovieId?.movieDirectorByMovieDirectorId,
    });

    useEffect(() => {
        getAllMovieReviews();
    }, [getAllMovieReviews]);

    return filterState;
}

export const GET_ALL_MOVIE_REVIEWS_QUERY = gql`
    query allMovieReviews {
        allMovieReviews {
            nodes {
                id
                title
                body
                rating
                movieByMovieId {
                    id
                    title
                    releaseDate
                    userByUserCreatorId {
                        id
                        name
                    }
                    movieDirectorByMovieDirectorId {
                        age
                        id
                        name
                    }
                }
                userByUserReviewerId {
                    name
                    id
                }
            }
        }
    }
`;

export default useMovieReviewsData;