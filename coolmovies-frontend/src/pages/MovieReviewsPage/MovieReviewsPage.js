import React from 'react';
import useMovieReviewsData, { REVIEW_ORDER_BY_OPTIONS } from "./hooks/useMovieReviewsData";
import { css } from "@emotion/react";
import MovieReviewList from "../../components/moviereview/MovieReviewList";
import Filters from "../../components/filters/Filters";

const MovieReviewsPage = () => {
    const { list: movieReviews, updateFilters, resetFilters, loading, filters } = useMovieReviewsData();

    return (
        <div css={style}>
            <Filters
                updateFilters={updateFilters}
                resetFilters={resetFilters}
                filters={filters}
                css={filtersStyle}
                title={'Filter movie reviews'}
                orderByOptions={REVIEW_ORDER_BY_OPTIONS}
            />
            <MovieReviewList
                movieReviews={movieReviews}
                loading={loading}
            />
        </div>
    )
};

const style = css`
    width: 100%;
    display: flex;
    justify-content: center;
    flex-direction: column;
    margin: 0 auto 30px;
`;

const filtersStyle = css`
    margin-bottom: 30px;
`;

export default MovieReviewsPage;