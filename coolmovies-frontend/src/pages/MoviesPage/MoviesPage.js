import React from 'react';
import useMoviesData, { MOVIE_ORDER_BY_OPTIONS } from "./hooks/useMoviesData";
import { css } from "@emotion/react";
import MovieList from "../../components/movie/MovieList";
import Filters from "../../components/filters/Filters";

const MoviesPage = () => {
    const { list: movies, updateFilters, resetFilters, loading, filters } = useMoviesData();

    return (
        <div css={style}>
            <Filters
                updateFilters={updateFilters}
                resetFilters={resetFilters}
                filters={filters}
                css={filtersStyle}
                title={'Filter movies'}
                orderByOptions={MOVIE_ORDER_BY_OPTIONS}
            />
            <MovieList
                movies={movies}
                loading={loading}
            />
        </div>
    )
};

const style = css`
    width: 100%;
    display: flex;
    justify-content: center;
    flex-direction: column;
    margin: 0 auto 50px;
`;

const filtersStyle = css`
    margin-bottom: 20px;
`;

export default MoviesPage;