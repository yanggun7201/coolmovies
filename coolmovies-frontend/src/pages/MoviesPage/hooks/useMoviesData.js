import { useEffect, useMemo } from "react";
import { gql, useLazyQuery } from "@apollo/client";
import { compareDate, compareText, createSortableItem } from "../../../core/includes/sort";
import useFilters from "../../../core/hooks/useFilters";

export const MOVIE_ORDER_BY_OPTIONS = Object.freeze([
    Object.freeze(createSortableItem({
        id: "title",
        value: "Title",
        sortField: "title",
        defaultValue: "",
        sortDirection: "asc",
        compare: compareText,
    })),
    Object.freeze(createSortableItem({
        id: "released_newest",
        value: "Newest by release date",
        sortField: "releaseDate",
        defaultValue: "1970-01-01",
        sortDirection: "desc",
        compare: compareDate,
    })),
    Object.freeze(createSortableItem({
        id: "released_oldest",
        value: "Oldest by release date",
        sortField: "releaseDate",
        defaultValue: "1970-01-01",
        sortDirection: "asc",
        compare: compareDate,
    })),
]);


const useMoviesData = () => {
    const [getAllMovies, { data }] = useLazyQuery(GET_ALL_MOVIES_QUERY);

    const movies = useMemo(() => {
        if (data) {
            return data?.allMovies?.nodes?.slice();
        }
        return [];
    }, [data]);

    const filterState = useFilters(movies, MOVIE_ORDER_BY_OPTIONS, {
        getTitle: movie => movie.title,
        getDirector: movie => movie?.movieDirectorByMovieDirectorId,
    });

    useEffect(() => {
        getAllMovies();
    }, [getAllMovies]);

    return filterState;
}

const GET_ALL_MOVIES_QUERY = gql`
    query getAllMovies {
        allMovies {
            nodes {
                id
                title
                releaseDate
                movieDirectorByMovieDirectorId {
                    id
                    name
                }
            }
        }
    } 
`;

export default useMoviesData;