import React, { useCallback, useMemo, useState } from "react";
import { Redirect, useHistory, useParams } from "react-router-dom";
import { gql, useMutation } from "@apollo/client";
import { css } from "@emotion/react";
import { errorMessage } from "../../core/hooks/useToast";
import { useCurrentUserContext } from "../../core/contexts/CurrentUserContext";
import useMovieReviewDetailData from "./hooks/useMovieReviewDetailData";
import StarRating from "../../components/rating/StarRating";
import ReviewerLink from "../../components/links/ReviewerLink";
import Loading from "../../components/loading/Loading";
import DirectorLink from "../../components/links/DirectorLink";
import Button from "../../components/form/Button";
import MovieReviewCommentList from "../../components/comment/MovieReviewCommentList";
import DeletionConfirmDialog from "../../components/dialog/DeletionConfirmDialog";
import Title from "../../components/form/Title";
import SubTitle from "../../components/form/SubTitle";
import TitleContainer from "../../components/form/TitleContainer";
import LinkAboveTitle from "../../components/links/LinkAboveTitle";
import { GET_ALL_MOVIE_REVIEWS_QUERY } from "../MovieReviewsPage/hooks/useMovieReviewsData";

const MovieReviewDetailPage = () => {
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [modalIsOpen, setModalIsOpen] = useState(false);
    const history = useHistory();
    const { reviewId } = useParams();
    const { movieReview, loading } = useMovieReviewDetailData(reviewId);
    const { user } = useCurrentUserContext();
    const [deleteMovieReview] = useMutation(DELETE_MOVIE_REVIEW_QUERY);

    const onCloseModal = useCallback(() => {
        setModalIsOpen(false);
    }, []);

    const onOpenConfirm = useCallback(() => {
        setModalIsOpen(true);
    }, []);

    const onDeleteConfirmed = useCallback(() => {
        const movieReviewInput = {
            nodeId: movieReview.nodeId,
        }

        setIsSubmitting(true);

        return deleteMovieReview({
            variables: {
                input: movieReviewInput,
            },
            awaitRefetchQueries: true,
            refetchQueries: [{ query: GET_ALL_MOVIE_REVIEWS_QUERY }],
        }).then(() => {
            history.push(`/reviews`);
        }).catch(error => {
            errorMessage("Review deletion error.");
            setIsSubmitting(false);
        });
    }, [movieReview, history, deleteMovieReview]);

    const isEditable = useMemo(() => {
        if (movieReview && user) {
            return movieReview.userByUserReviewerId?.id === user.id;
        }
        return false;
    }, [user, movieReview]);

    if (!reviewId || (!loading && !movieReview)) {
        return <Redirect to={"/"} />;
    }

    return (
        <div css={style}>
            {loading
                ? (<Loading onTop overlay />)
                : (
                    <>
                        <TitleContainer>
                            <LinkAboveTitle to={"/reviews"}>Reviews</LinkAboveTitle>
                            <Title>{movieReview.movieByMovieId.title}</Title>
                            <div>
                                <span>Director:&nbsp;</span>
                                <DirectorLink css={reviewerStyle} user={movieReview.movieByMovieId.movieDirectorByMovieDirectorId} />
                            </div>
                        </TitleContainer>

                        <TitleContainer>
                            <SubTitle>{movieReview.title}</SubTitle>
                            <StarRating rating={movieReview.rating} starDimension={"20px"} />
                            <div css={reviewerContainerStyle}>
                                <span>Reviewer:&nbsp;</span>
                                <ReviewerLink css={reviewerStyle} user={movieReview.userByUserReviewerId} />
                            </div>
                            <div css={reviewBodyStyle}>
                                {movieReview.body}
                            </div>
                        </TitleContainer>

                        {isEditable && (
                            <div css={buttonsContainerStyle}>
                                <Button to={`/reviews/${movieReview.id}/edit`}>
                                    Edit review
                                </Button>
                                <Button onClick={onOpenConfirm} variant={"cancel"}>
                                    Delete review
                                </Button>
                            </div>
                        )}

                        <MovieReviewCommentList
                            movieReviewId={movieReview.id}
                            movieReviewComments={movieReview?.commentsByMovieReviewId?.nodes}
                        />
                    </>
                )
            }

            {modalIsOpen && (
                <DeletionConfirmDialog
                    onCloseClicked={onCloseModal}
                    onSaveClicked={onDeleteConfirmed}
                    loading={isSubmitting}
                >
                    Do you want to delete this review?
                </DeletionConfirmDialog>
            )}
        </div>
    );
};

const DELETE_MOVIE_REVIEW_QUERY = gql`
    mutation ($input: DeleteMovieReviewInput!) {
        deleteMovieReview(input: $input) {
            deletedMovieReviewId
        }
    }
`;

const style = css`
    display: flex;
    justify-content: center;
    flex-direction: column;
    width: 100%;
    min-width: 300px;
    max-width: 600px;
    margin: 0 auto 30px;
    padding: 0 12px;
`;

const reviewerStyle = css`
    margin-right: 10px;
`;

const reviewBodyStyle = css`
    white-space: pre-line;
    margin-bottom: 30px;
`;

const reviewerContainerStyle = css`
    margin-bottom: 10px;
`;

const buttonsContainerStyle = css`
    display: flex;
    flex-direction: row;
    margin-bottom: 30px;

    > * {
        margin-right: 10px;
    }
`;

export default MovieReviewDetailPage;