import React from "react";
import PropTypes from "prop-types";
import { css } from "@emotion/react";
import ReviewerLink from "../../../components/links/ReviewerLink";

const Author = ({ author, className = '' }) => {
    return (
        <div css={style} className={className}>
            <ReviewerLink reviewer={author} />
        </div>
    )
}

Author.propTypes = {
    author: PropTypes.object.isRequired,
    className: PropTypes.string,
}

const style = css`

`;

export default Author;