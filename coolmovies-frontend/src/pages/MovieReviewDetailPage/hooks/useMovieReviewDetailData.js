import { useEffect } from "react";
import { gql, useLazyQuery } from "@apollo/client";
import useSetState from "../../../core/hooks/useSetState";

const DEFAULT_STATE = {
    movieReview: null,
    loading: true,
};

const useMovieReviewDetailData = (id) => {
    const [state, setState] = useSetState(DEFAULT_STATE);
    const [getMovieReview, { data, error }] = useLazyQuery(GET_MOVIE_REVIEW_QUERY);

    useEffect(() => {
        if (getMovieReview && id) {
            getMovieReview({
                variables: {
                    id
                }
            });
        }
    }, [getMovieReview, id]);

    useEffect(() => {
        if (data) {
            setState({ movieReview: data.movieReviewById, loading: false });
        }
    }, [data, setState]);

    useEffect(() => {
        if (error) {
            setState({ movieReview: null, loading: false });
        }
    }, [error, setState]);

    return state;
}

const GET_MOVIE_REVIEW_QUERY = gql`
    query movieReviewByIdInDetail ($id: UUID!) {
        movieReviewById (id: $id) {
            body
            id
            title
            nodeId
            movieByMovieId {
                id
                releaseDate
                title
                movieDirectorByMovieDirectorId {
                    age
                    id
                    name
                }
            }
            rating
            nodeId
            title
            userByUserReviewerId {
                name
                id
            }
            commentsByMovieReviewId {
                totalCount
                nodes {
                    nodeId
                    id
                    body
                    title
                    userByUserId {
                        id
                        name
                    }
                }
            }
        }
    }
`;

export default useMovieReviewDetailData;