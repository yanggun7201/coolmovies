import { gql } from "@apollo/client";
import useMovieData from "../../../core/hooks/useMovieData";

const useMovieDetailData = (id) => {
    return useMovieData(id, GET_MOVIE_WITH_REVIEWS_QUERY);
}

const GET_MOVIE_WITH_REVIEWS_QUERY = gql`
    query getMovie ($id: UUID!) { 
        movieById(id: $id) {
            id
            title
            releaseDate
            movieDirectorByMovieDirectorId {
                id
                name
            }
            movieReviewsByMovieId {
                nodes {
                    id
                    title
                    rating
                    userByUserReviewerId {
                        id
                        name
                    }
                }
            }
        }
    }
`;

export default useMovieDetailData;