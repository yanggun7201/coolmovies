import React, { useMemo } from "react";
import { Redirect, useParams } from "react-router-dom";
import Loading from "../../components/loading/Loading";
import { css } from "@emotion/react";
import useMovieDetailData from "./hooks/useMovieDetailData";
import DirectorLink from "../../components/links/DirectorLink";
import MovieReviewList from "../../components/moviereview/MovieReviewList";
import { useCurrentUserContext } from "../../core/contexts/CurrentUserContext";
import Button from "../../components/form/Button";
import Title from "../../components/form/Title";
import SubTitle from "../../components/form/SubTitle";
import TitleContainer from "../../components/form/TitleContainer";
import LinkAboveTitle from "../../components/links/LinkAboveTitle";

const MovieDetailPage = () => {
    const { movieId } = useParams();
    const { loading, movie } = useMovieDetailData(movieId);
    const { user } = useCurrentUserContext();

    const movieReviews = useMemo(() => {
        if (!movie?.movieReviewsByMovieId?.nodes) return [];
        return movie?.movieReviewsByMovieId?.nodes.map(review => review);
    }, [movie]);

    const canCreateReview = useMemo(() => {
        if (movieReviews && user) {
            return !movieReviews.some(review => review.userByUserReviewerId?.id === user.id);
        }
        return true;
    }, [movieReviews, user]);

    if (!movieId || (!loading && !movie)) {
        return <Redirect to={"/movies"} />;
    }

    return (
        <div css={style}>
            {loading
                ? (<Loading onTop overlay />)
                : (
                    <>
                        <TitleContainer>
                            <LinkAboveTitle to={"/movies"}>Movies</LinkAboveTitle>
                            <Title>{movie.title}</Title>
                            <div>
                                <span>Director:&nbsp;</span>
                                <DirectorLink css={reviewerStyle} user={movie.movieDirectorByMovieDirectorId} />
                            </div>
                            <div>
                                <span>Release date:&nbsp;</span>
                                <span>{movie.releaseDate}</span>
                            </div>
                            {canCreateReview && (
                                <Button to={`/movies/${movieId}/create-review`}>
                                    Create review
                                </Button>
                            )}
                        </TitleContainer>

                        <SubTitle>Reviews</SubTitle>
                        <MovieReviewList
                            movieReviews={movieReviews}
                            loading={loading}
                        />
                    </>
                )
            }
        </div>
    );
}

const style = css`
    display: flex;
    justify-content: center;
    flex-direction: column;
    width: 100%;
    margin: 0 auto 30px;
    padding: 0 12px;
`;

const reviewerStyle = css`
    margin-right: 10px;
`;

export default MovieDetailPage;