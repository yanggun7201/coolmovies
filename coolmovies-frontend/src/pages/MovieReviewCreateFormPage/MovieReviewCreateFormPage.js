import React, { useEffect } from "react";
import { Redirect, useHistory, useParams } from "react-router-dom";
import { gql, useMutation } from "@apollo/client";
import useMovieDataForReview from "./hooks/useMovieDataForReview";
import MovieReviewCreateUpdateForm from "../MovieReviewUpdateFormPage/components/MovieReviewCreateUpdateForm";

const MovieReviewCreateFormPage = () => {
    const { movieId } = useParams();
    const history = useHistory();
    const { loading, movie } = useMovieDataForReview(movieId);
    const [createMovieReview, { data }] = useMutation(CREATE_MOVIE_REVIEW_QUERY)

    useEffect(() => {
        if (data) {
            history.push(`/reviews/${data.createMovieReview?.movieReview?.id}`);
        }
    }, [data, history]);

    if (!movieId || (!loading && !movie)) {
        return <Redirect to={"/"} />;
    }

    return (
        <MovieReviewCreateUpdateForm
            movie={movie}
            createUpdateMovieReviewMutation={createMovieReview}
            loading={loading}
        />
    );
}

const CREATE_MOVIE_REVIEW_QUERY = gql`
    mutation ($input: CreateMovieReviewInput!) {
        createMovieReview(input: $input) {
            movieReview {
                id
                title
                body
                rating
                movieByMovieId {
                    title
                }
                userByUserReviewerId {
                    name
                }
            }
        }
    }
`;

export default MovieReviewCreateFormPage;