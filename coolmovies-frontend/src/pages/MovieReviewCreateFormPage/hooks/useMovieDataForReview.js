import { gql } from "@apollo/client";
import useMovieData from "../../../core/hooks/useMovieData";

const useMovieDataForReview = (id) => {
    return useMovieData(id, GET_MOVIE_QUERY);
}

const GET_MOVIE_QUERY = gql`
    query getMovie ($id: UUID!) { 
        movieById(id: $id) {
            id
            title
            releaseDate
            movieDirectorByMovieDirectorId {
                id
                name
            }
        }
    }
`;

export default useMovieDataForReview;