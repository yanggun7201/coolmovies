import React from 'react';
import ReactDOM from 'react-dom';
import { ThemeProvider } from "@emotion/react";
import { ApolloProvider } from "@apollo/client";
import reportWebVitals from './reportWebVitals';
import App from './App';
import { theme } from "./theme";
import apolloClient from "./core/includes/apollo";
import { CurrentUserContextProvider } from "./core/contexts/CurrentUserContext";

ReactDOM.render(
    <React.StrictMode>
        <ThemeProvider theme={theme}>
            <ApolloProvider client={apolloClient}>
                <CurrentUserContextProvider>
                    <App />
                </CurrentUserContextProvider>
            </ApolloProvider>
        </ThemeProvider>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
