import colourUtils from "./colourUtils";

const grey = {
    50: '#fafafa',
    80: '#F3F3F3',
    90: '#F4F4F4',
    100: '#f5f5f5',
    200: '#eeeeee',
    300: '#e0e0e0',
    400: '#bdbdbd',
    450: '#AAAAAA',
    500: '#9e9e9e',
    600: '#757575',
    650: '#707070',
    700: '#757575',
    720: '#555555',
    800: '#424242',
    900: '#212121',
    920: '#1B1D28',
};

const red = {
    50: '#fff2f2',
    100: '#ffcdd2',
    200: '#ef9a9a',
    300: '#e57373',
    350: '#DC676C',
    400: '#ef5350',
    500: '#f44336',
    600: '#e53935',
    700: '#d32f2f',
    800: '#c62828',
    900: '#b71c1c',
    999: '#ff0000',
};

const colours = {
    transparent: 'rgba(0,0,0,0)', // Useful for transitions between no fill and fill.,
    white: '#ffffff',
    text: '#212121',
    defaultText: '#6a7380',
    defaultText2: '#6B7280',
    blackGrayText: '#121212',
    link: '#3c81f6',
    background: '#ffffff',
    border: '#AAAAAA',
    eco: '#4ba26d',
    eco2: '#56a757',
    crimson: '#ED1C24',
    soapStone: '#FFF9F7',
    hawkesBlue: '#d2e2fe',
    cornflower: '#98b6e4',
    solitude: '#e3effe',
    doveGray2: '#666666',
    grey,
    red,
    utils: colourUtils,
};

export default {
    ...colours,
};