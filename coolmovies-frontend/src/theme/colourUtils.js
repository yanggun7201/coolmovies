import { darken } from 'polished';

const hexToInt = (hex) => parseInt(parseInt(hex, 16).toString(10));

const rgba = (hex, alpha = 1) => {
    if (!hex || hex.length !== 7) {
        throw Error(`Hex colour must have length of 7, got ${hex}`);
    }

    const rgb = [hex.substring(1, 3), hex.substring(3, 5), hex.substring(5, 7)];

    return `rgba(${hexToInt(rgb[0])}, ${hexToInt(rgb[1])}, ${hexToInt(rgb[2])}, ${alpha})`;
};

const utils = {
    hexToInt,
    rgba,
    darken,
};


export default {
    ...utils,
};
