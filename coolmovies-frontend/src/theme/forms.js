import colours from "./colours";
import { css } from "@emotion/react";

const forms = {
    error: css`
        border: 1px solid ${colours.crimson};
        background-color: ${colours.soapStone};
    `,
};

export default forms;