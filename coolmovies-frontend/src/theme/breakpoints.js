const breakpoints = {
    sizes: {
        'xxs': 0,
        'xs': 368,
        'sm': 640,
        'md': 768,
        'lg': 1024,
        'xl': 1280,
        'xxl': 1440,
        '3xl': 1600,
        '4xl': 1900,
        '5xl': 2200,
    },
    order: [
        'xxs',
        'xs',
        'sm',
        'md',
        'lg',
        'xl',
        'xxl',
        '3xl',
        '4xl',
        '5xl'
    ],
    breakpointsToScale: [
        '3xl',
        '4xl',
        '5xl',
    ],
    scale: {
        '3xl': 1.1,
        '4xl': 1.18,
        '5xl': 1.26
    }
};

export const getNextBreakpoint = (size) => breakpoints.order[breakpoints.order.indexOf(size) + 1];
const getBreakpointMin = (size) => breakpoints.sizes[size];
const getBreakpointMax = (size) => breakpoints.sizes[getNextBreakpoint(size)] - .02;

/**
 * Returns the media query for a single breakpoint.
 *
 * min represents the breakpoint matching the size prop.
 * max represents the next highest breakpoint from the size prop, minus .02.
 *
 * Example use:
 * ${theme.breakpoints.down('lg')} {
 *     height: 100px;
 * }
 * @param size
 * @returns {string}
 */
export const breakpointOnly = (size) => {
    const min = getBreakpointMin(size);
    const max = getBreakpointMax(size);
    return `@media (min-width: ${min}px)${max ? ` and (max-width: ${max}px)` : ""}`;
};

/**
 * Returns the media query for a minimum breakpoint.
 *
 * min represents the breakpoint matching the size prop.
 *
 * Example use:
 * ${theme.breakpoints.up('lg')} {
 *     height: 100px;
 * }
 * @param size
 * @returns {string}
 */
export const breakpointUp = (size) => `@media (min-width: ${getBreakpointMin(size)}px)`;

/**
 * Returns the media query for a maximum breakpoint.
 *
 * max represents the next highest breakpoint from the size prop, minus .02.
 *
 * Example use:
 * ${theme.breakpoints.down('lg')} {
 *     height: 100px;
 * }
 * @param size
 * @returns {string}
 */
export const breakpointDown = (size) => {
    const max = getBreakpointMax(size);
    return max ? `@media (max-width: ${max}px)` : `@media (min-width: 0)`;
};

/**
 * Returns the media query for a variable number of breakpoints.
 *
 * min represents the breakpoint matching the low prop.
 * max represents the breakpoint matching the next highest breakpoint from the high prop, minus .02.
 *
 * Example use:
 * ${theme.breakpoints.between('lg', 'xl')} {
 *     height: 100px;
 * }
 * @param low
 * @param high
 * @returns {string}
 */
export const breakpointBetween = (low, high) => {
    const min = getBreakpointMin(low);
    const max = getBreakpointMax(high);

    return `@media (min-width: ${min}px) ${max ? ` and (max-width: ${max}px)` : ""}`;
};

export default {
    ...breakpoints,
    only: breakpointOnly,
    between: breakpointBetween,
    up: breakpointUp,
    down: breakpointDown,
};
