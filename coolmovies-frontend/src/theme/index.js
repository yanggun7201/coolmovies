import breakpoints from "./breakpoints";
import colours from './colours';
import transitions from "./transitions";
import borderAndShadow from "./borderAndShadow";
import zIndex from './zIndex';
import mixins from "./mixins";
import forms from "./forms";

export const theme = {
    breakpoints,
    transitions,
    borderAndShadow,
    zIndex,
    mixins,
    colours,
    forms,
};