import transitions from './transitions';
import colours from "./colours";
import { css } from "@emotion/react";

const placeholderColor = "#9ca3af";

const mixins = {
    bloop: `
        transition: transform ${transitions.transitionFastest};
        &:hover {
            transform: scale(1.1);
        }
    `,
    bloopScale: `
        transform: scale(1.1);
    `,
    placeholderColor: placeholderColor,
    placeholderAttributes: (content) => `
        color: ${placeholderColor};
        opacity: 1; /** Safari / Firefox **/

        ${content};
    `,

    placeholder: (content) => `
        &::placeholder { /** Chrome **/
            ${mixins.placeholderAttributes(content)};

            /** We put @content after the above include, so that the @content can override **/
            ${content};
        }

        /** This css doesn't work with the list below **/
        &::-webkit-input-placeholder, /** Opera/Safari **/
        &::-moz-placeholder, /** Firefox 19+ **/
        &:-moz-placeholder { /** Firefox 18- **/
            ${mixins.placeholderAttributes(content)};

            /** We put @content after the above include, so that the @content can override **/
            ${content};
        }

        /** IE: This css doesn't work with the list above **/
        &:-ms-input-placeholder {
            ${mixins.placeholderAttributes(content)};

            /** We put @content after the above include, so that the @content can override **/
            ${content};
        }

        /** Edge: This css doesn't work with the list above **/
        &::-ms-input-placeholder {
            ${mixins.placeholderAttributes(content)};

            /** We put @content after the above include, so that the @content can override **/
            ${content};
        }
    `,
    input: (backgroundColor) => `
        input,
        input:-webkit-autofill,
        input:-webkit-autofill:hover,
        input:-webkit-autofill:focus,
        input:-webkit-autofill:active {
            background-color: ${backgroundColor};
            -webkit-box-shadow: 0 0 0 30px ${backgroundColor} inset;
        }
    `,
};

const DEFAULT_UNDERLINE_COLOUR = colours.link;

const animatedUnderlineContainerStyle = (underlineColour = DEFAULT_UNDERLINE_COLOUR, extraStyle) => css`
    display: inline-block;
    position: relative;
    cursor: pointer;

    &:after {
        background: none repeat scroll 0 0;
        bottom: 2px;
        content: "";
        display: block;
        height: 1px;
        left: 50%;
        position: absolute;
        background: ${underlineColour};
        transition: width ${transitions.transition}, left ${transitions.transition};
        width: 0;
    }
    
    ${extraStyle && extraStyle};
`;

const animatedUnderlineHoverAfterStyle = css`
    &:after {
        width: 100%;
        left: 0;
    }
`;

const animatedUnderlineContainer = ({ className = 'hover-underline', underlineColour }) => css`
    & .${className} {
        ${animatedUnderlineContainerStyle(underlineColour)};
    }

    &:hover {
        .${className} {
            pointer-events: none;
            ${animatedUnderlineHoverAfterStyle};
        }
    }
`;

const animatedUnderlineItSelf = ({ underlineColour, extraStyle } = {}) => css`
    ${animatedUnderlineContainerStyle(underlineColour, extraStyle)};

    &:hover {
        ${animatedUnderlineHoverAfterStyle};
    }
`;

export default {
    ...mixins,
    animatedUnderlineContainer,
    animatedUnderlineItSelf,
};
