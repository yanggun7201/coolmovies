const toastDuration = 5000;

export const defaultToastOptions = {
    position: "top-right",
    autoClose: toastDuration,
    newestOnTop: true,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: false,
    draggable: true,
};

export const errorToastOptions = {
    ...defaultToastOptions,
};