import React from "react";
import { css } from "@emotion/react";
import { Redirect, Route, Switch, useHistory, useLocation } from "react-router-dom";
import { CSSTransition, SwitchTransition, TransitionGroup } from "react-transition-group";

import MovieReviewsPage from "../../pages/MovieReviewsPage";
import MovieReviewDetailPage from "../../pages/MovieReviewDetailPage";
import MovieReviewerDetailPage from "../../pages/MovieReviewerDetailPage";
import getTransitionKey from "../includes/transition";
import MovieDirectorDetailPage from "../../pages/MovieDirectorDetailPage";
import MoviesPage from "../../pages/MoviesPage";
import MovieDetailPage from "../../pages/MovieDetailPage";
import MovieReviewCreateFormPage from "../../pages/MovieReviewCreateFormPage";
import MovieReviewUpdateFormPage from "../../pages/MovieReviewUpdateFormPage";

const routes = [
    { path: '/movies', name: "movies", Component: MoviesPage },
    { path: '/movies/:movieId', name: "movie", Component: MovieDetailPage },
    { path: '/movies/:movieId/create-review', name: "create-review", Component: MovieReviewCreateFormPage },
    { path: '/reviews', name: "reviews", Component: MovieReviewsPage },
    { path: '/reviews/:reviewId', name: "review", Component: MovieReviewDetailPage },
    { path: '/reviews/:reviewId/edit', name: "update-review", Component: MovieReviewUpdateFormPage },
    { path: '/reviewers/:id', name: "reviewer", Component: MovieReviewerDetailPage },
    { path: '/directors/:id', name: "director", Component: MovieDirectorDetailPage },
    { path: '/', name: "", Component: MovieReviewsPage },
];

const Routes = () => {
    const location = useLocation();
    const history = useHistory();
    const transitionKey = getTransitionKey(location);
    const nodeRef = React.useRef(null);

    return (
        <TransitionGroup exit={false}>
            <CSSTransition
                nodeRef={nodeRef}
                classNames="page"
                timeout={{ enter: 300 }}
                key={transitionKey}
            >
                <section css={bodyContainerStyle}>
                    <Switch>
                        {routes.map(({ path, Component }) => (
                            <Route key={path} exact path={path}>
                                <Component />
                            </Route>
                        ))}
                        <Route
                            path=""
                            render={() => (
                                <SwitchTransition action={history.action}>
                                    <Redirect to="/reviews" />
                                </SwitchTransition>
                            )}
                        />
                    </Switch>
                </section>
            </CSSTransition>
        </TransitionGroup>
    )
}

const bodyContainerStyle = css`
    display: flex;
    flex-direction: column;
    width: 100%;
    position: relative;
`;

export default Routes;