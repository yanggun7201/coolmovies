import React, { createContext, useContext, useMemo, useState } from "react";
import * as PropTypes from "prop-types";

const CurrentUserContext = createContext(null);

export const CurrentUserContextProvider = ({ children }) => {

    const [user, setUser] = useState(false);

    const value = useMemo(() => {
        return {
            user,
            setUser,
        }
    }, [user]);

    return (
        <CurrentUserContext.Provider value={value}>
            {children}
        </CurrentUserContext.Provider>
    );
}

CurrentUserContextProvider.propTypes = {
    children: PropTypes.node
};

export const useCurrentUserContext = () => {
    return useContext(CurrentUserContext);
};