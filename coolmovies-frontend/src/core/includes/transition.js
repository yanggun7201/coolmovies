export function generateTransitionKey(pathname) {
    return pathname.replace(/[0-9]/g, '').replace(/\/$/, '');
}

export function getTransitionKey(location) {
    return generateTransitionKey(location.pathname);
}

export default getTransitionKey;