import { ApolloError } from "@apollo/client";

export const getMessageFromError = (error) => {
    let message = '';

    // check for a graphql
    if (error instanceof ApolloError) {
        if (error.graphQLErrors && error.graphQLErrors.length) {
            message = error.graphQLErrors[0].message;
        } else {
            message = error.message;
        }

        // generic error
    } else if (error instanceof Error) {
        message = error.message;

        // default, assume string
    } else {
        message = error;
    }

    // don't show internal server errors or network errors
    if (message === 'Internal server error' || message.toLowerCase().indexOf('network error') !== -1) {
        message = 'An error has occurred';
    }

    return message;
};

export default getMessageFromError;
