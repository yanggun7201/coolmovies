import PropTypes from "prop-types";

export const extraCss = (extra, ...args) => typeof extra === "function" ? extra(...args) : extra;

export const extraCssPropType = PropTypes.oneOfType([PropTypes.object, PropTypes.func]);

export default {
    extraCss,
    extraCssPropType,
};
