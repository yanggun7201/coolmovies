const MOBILE_WIDTH = 768;

export const preventBodyScrolling = () => {
    document.body.style.overflow = 'hidden';
    document.body.style.position = 'fixed';
    document.body.style.width = '100%';
}

export const enableBodyScrolling = () => {
    document.body.style.removeProperty('overflow');
    document.body.style.removeProperty('position');
    document.body.style.removeProperty('width');
}

export const windowWidth = () => {
    return window.innerWidth || document.documentElement.clientWidth;
}

export const isMobile = () => {
    return windowWidth() < MOBILE_WIDTH;
}