import {
    InMemoryCache,
    HttpLink,
    ApolloClient,
    ApolloLink,
} from "@apollo/client";

const defaultOptions = {
    watchQuery: {
        fetchPolicy: 'network-only',
        errorPolicy: 'none',
    },
    query: {
        fetchPolicy: 'network-only',
        errorPolicy: 'none',
    },
};

const createClient = () => new ApolloClient({
    link: ApolloLink.from([
        new HttpLink({
            uri: `/graphql`,
        })
    ]),
    cache: new InMemoryCache(),
    defaultOptions: defaultOptions,
});

export default createClient();
