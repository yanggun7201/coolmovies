import moment from "moment";
import { get, isEmpty, isNumber } from "lodash";

const _compareDate = (aValue, bValue) => {
    if (aValue.isAfter(bValue)) return 1;
    if (aValue.isBefore(bValue)) return -1;
    return 0;
};

const _compareNumber = (aValue, bValue) => {
    if (aValue > bValue) return 1;
    if (aValue < bValue) return -1;
    return 0;
};

const _getDirection = (sortDirection) => sortDirection !== "asc" ? -1 : 1;

const _getValue = (item, sortFields, valueIsNumber = false) => {
    for (let i = 0; i < sortFields.length; i++) {
        const value = get(item, sortFields[i]);
        if (value || (valueIsNumber && isNumber(value))) return value;
    }
    return null;
}

export function compareDate(sortFields, defaultValue) {
    return (a, b, sortDirection) => {
        const aValue = moment(_getValue(a, sortFields) || defaultValue);
        const bValue = moment(_getValue(b, sortFields) || defaultValue);
        return _compareDate(aValue, bValue) * _getDirection(sortDirection);
    }
}

export function compareText(sortFields, defaultValue) {
    return (a, b, sortDirection) => {
        const aValue = _getValue(a, sortFields) || defaultValue;
        const bValue = _getValue(b, sortFields) || defaultValue;
        return aValue.localeCompare(bValue) * _getDirection(sortDirection);
    }
}

export function compareNumber(sortFields, defaultValue) {
    return (a, b, sortDirection) => {
        const aValue = _getValue(a, sortFields, true) ?? defaultValue;
        const bValue = _getValue(b, sortFields, true) ?? defaultValue;
        return _compareNumber(aValue, bValue) * _getDirection(sortDirection);
    }
}

export function createSortableItem(item = {}) {
    switch (item.compare) {
        case compareNumber:
        case compareText:
        case compareDate:
            if (!isEmpty(item.sortFields)){
                return {
                    ...item,
                    compare: item.compare(item.sortFields, item.defaultValue)
                };
            }

            return {
                ...item,
                compare: item.compare([item.sortField], item.defaultValue)
            };

        default:
            return item;
    }
}
