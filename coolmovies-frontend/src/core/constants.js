
export const DEFAULT_SAVE_TIME = 1000;

export const NOOP = () => {};

export const RATING_OPTIONS = Object.freeze([
    Object.freeze({ id: 1, value: "½", }),
    Object.freeze({ id: 2, value: "★", }),
    Object.freeze({ id: 3, value: "★½", }),
    Object.freeze({ id: 4, value: "★★", }),
    Object.freeze({ id: 5, value: "★★½", }),
    Object.freeze({ id: 6, value: "★★★", }),
    Object.freeze({ id: 7, value: "★★★½", }),
    Object.freeze({ id: 8, value: "★★★★", }),
    Object.freeze({ id: 9, value: "★★★★½", }),
    Object.freeze({ id: 10, value: "★★★★★", }),
]);