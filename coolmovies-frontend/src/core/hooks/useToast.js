import { toast } from "react-toastify";
import { getMessageFromError } from '../includes/errors';
import { defaultToastOptions, errorToastOptions } from "../config/config";

export const successMessage = (message) => toast.success(message, defaultToastOptions);

export const errorMessage = (error) => {
    const message = getMessageFromError(error);
    toast.error(message, errorToastOptions);
};

export const infoMessage = (message) => toast.info(message, defaultToastOptions);

const useToast = () => {
    return [successMessage, errorMessage, infoMessage];
};

export default useToast;
