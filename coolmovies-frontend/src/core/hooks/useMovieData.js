import { useEffect } from "react";
import { gql, useLazyQuery } from "@apollo/client";
import useSetState from "./useSetState";

const DEFAULT_STATE = {
    movie: null,
    loading: true,
};

const useMovieData = (id, query = GET_REVIEWER_QUERY) => {
    const [state, setState] = useSetState(DEFAULT_STATE);
    const [getMovie, { data }] = useLazyQuery(query);

    useEffect(() => {
        getMovie({
            variables: {
                id,
            }
        });
    }, [getMovie, id]);

    useEffect(() => {
        if (data) {
            setState({ movie: data.movieById, loading: false });
        }
    }, [data, setState]);

    return state;
}

const GET_REVIEWER_QUERY = gql`
    query getMovie ($id: UUID!) { 
        movieById(id: $id) {
            id
            title
            releaseDate
            movieDirectorByMovieDirectorId {
                id
                name
            }
            movieReviewsByMovieId {
                nodes {
                    id
                    title
                    rating
                    userByUserReviewerId {
                        id
                        name
                    }
                }
            }
        }
    }
`;

export default useMovieData;