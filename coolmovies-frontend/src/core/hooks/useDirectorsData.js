import { gql, useQuery } from "@apollo/client";
import { isEmpty } from "lodash";

const ALL_DIRECTOR_SELECTED = {
    value: "All",
}

const useDirectorsData = () => {
    const { data, loading } = useQuery(GET_ALL_DIRECTORS_QUERY);

    if (loading) {
        return [];
    }

    const list = data?.allMovieDirectors?.nodes?.map(director => {
        return {
            ...director,
            value: director.name,
        }
    }) ?? [];

    if (!isEmpty(list)) {
        return [
            ALL_DIRECTOR_SELECTED,
            ...list,
        ]
    }

    return list;
}

const GET_ALL_DIRECTORS_QUERY = gql`
    query {
        allMovieDirectors(orderBy: NAME_ASC) {
            nodes {
                id
                name
            }
        }
    }
`;

export default useDirectorsData;