import { useCallback, useEffect } from "react";
import { isEmpty } from "lodash";
import useSetState from "./useSetState";
import useDebounce from "./useDebounce";
import { DEFAULT_SAVE_TIME } from "../constants";

const DEFAULT_STATE = {
    list: [],
    filters: {
        title: '',
        orderBy: null,
        director: null,
    },
    loading: true,
};

const useFilters = (list = [], sortOptions = [], { getTitle, getDirector } = {}) => {
    const [state, setState] = useSetState(DEFAULT_STATE);

    const findSortableItem = useCallback(id => {
        return sortOptions.find(sortableItem => sortableItem.id === id);
    }, [sortOptions]);

    const getData = useCallback(() => {
        return list.slice();
    }, [list]);

    useEffect(() => {
        const list = getData();
        if (!isEmpty(list)) {
            setState({ list, loading: false });
        }
    }, [getData, setState]);

    const resetFilters = useCallback(() => {
        setState({
            ...DEFAULT_STATE,
            list: getData() || [],
            loading: false,
        })
    }, [getData, setState]);

    const _updateFilters = useCallback((filterKeyValue) => {
        setState(prevState => {
            return {
                filters: {
                    ...prevState.filters,
                    ...filterKeyValue,
                },
                loading: true,
            }
        })
    }, [setState]);

    const updateFiltersWithDebounce = useDebounce(filterKeyValue => {
        _updateFilters(filterKeyValue);
    }, DEFAULT_SAVE_TIME, []);

    const updateFilters = useCallback((filterKeyValue, debounce = false) => {
        if (debounce) {
            return updateFiltersWithDebounce(filterKeyValue);
        }
        _updateFilters(filterKeyValue);
    }, [updateFiltersWithDebounce, _updateFilters]);

    useEffect(() => {
            const originalList = getData();
            if (isEmpty(originalList)) return;

            setState(prevState => {
                const { filters } = prevState;
                let filteredList = [...originalList];

                if (filters?.title && getTitle) {
                    const lowerCateFilterTitle = filters.title.toLowerCase();
                    filteredList = filteredList.filter(movie => {
                        return getTitle(movie).replaceAll(/\s/g, '').toLowerCase().indexOf(lowerCateFilterTitle) > -1;
                    });
                }

                if (filters?.director?.id && getDirector) {
                    filteredList = filteredList.filter(movie => {
                        return getDirector(movie)?.id === filters?.director?.id;
                    });
                }

                if (filters?.orderBy?.id) {
                    const option = findSortableItem(filters.orderBy.id);

                    if (option) {
                        filteredList.sort((a, b) => option.compare(a, b, option.sortDirection));
                    }
                }

                return { list: filteredList, loading: false };
            });
        },
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [state.filters, getData, setState, findSortableItem]
    );

    return {
        ...state,
        updateFilters,
        resetFilters,
    };
}

export default useFilters;