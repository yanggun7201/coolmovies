import { useCallback } from 'react';
import { debounce } from "lodash";
import { DEFAULT_SAVE_TIME } from "../constants";

const useDebounce = (
    callback,
    delay = DEFAULT_SAVE_TIME,
    deps,
    options = { trailing: true }
) => {

    // eslint-disable-next-line react-hooks/exhaustive-deps
    const debounceFn = useCallback(
        debounce(callback, delay, options),
        [delay, ...deps]
    );

    return debounceFn;
};

export default useDebounce;
